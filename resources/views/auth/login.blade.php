@extends("layouts.app-auth")

@section('head_title', 'Login' .' | '.config('settings.site_name') )

@section('head_url', Request::url())

@section("content")

<form method="POST" action="{{ route('login') }}">
    @csrf
    <div class="form-group mb-3">
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-email-83"></i></span>
            </div>
            <input class="form-control" placeholder="Email" type="email" name="email" value="{{ old('email') }}"
                required autofocus>
        </div>
    </div>
    <div class="form-group">
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
            </div>
            <input class="form-control" name="password" placeholder="Password" type="password" value="" required>
        </div>
    </div>
    <div class="custom-control custom-control-alternative custom-checkbox">
        <input class="custom-control-input" id="customCheckLogin" type="checkbox" name="remember"
            {{ old('remember') ? 'checked' : '' }}><label class="custom-control-label" for="customCheckLogin">
            <span class="text-muted">Remember me</span>
        </label>
    </div>
    <div class="text-center">
        <button type="submit" class="btn btn-danger my-4">Sign in</button>
    </div>
</form>

@endsection
