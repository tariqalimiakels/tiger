@extends("layouts.app-auth")

@section('head_title', 'Register' .' | '.config('settings.site_name') )
@section('head_url', Request::url())

@section("content")

<form method="POST" action="{{ route('register') }}">
    @csrf
    <div class="form-group">
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-hat-3"></i></span>
            </div>
            <input class="form-control" placeholder="Name" type="text" name="name"
                value="{{ old('name') }}" required autofocus>
        </div>
    </div>
    <div class="form-group">
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-email-83"></i></span>
            </div>
            <input class="form-control" placeholder="Email" type="email" name="email" value="{{ old('email') }}"
                required>
        </div>
    </div>
    <div class="form-group">
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-mobile-button"></i></span>
            </div>
            <input class="form-control" placeholder="Phone" type="phone" name="phone" value="{{ old('phone') }}"
                required>    
        </div>
        <small>03XXXXXXXXX</small>
    </div>
    <div class="form-group">
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
            </div>
            <input class="form-control" placeholder="Password" type="password" name="password" required value="{{ old('password') }}">
        </div>
    </div>
    <div class="form-group">
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
            </div>
            <input class="form-control" placeholder="Confirm Password" type="password" name="password_confirmation"
                required value="{{ old('password_confirmation') }}">
        </div>
    </div>
    <div class="form-group">
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-single-02"></i></span>
            </div>

            <select class="form-control" name="usertype">
                <option value="User" {{ old('usertype') == "User" ? 'selected' : '' }}>User</option>
                <option value="Owner" {{ old('usertype') == "Owner" ? 'selected' : '' }}>Shop Owner</option>
            </select>
        </div>
    </div>
    @if(Session::has('status'))
    <div class="form-group">
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
            </div>
            <input class="form-control" placeholder="Verification Code" type="number" name="verification_code"
                required>
        </div>
    </div>
    @endif
    <div class="text-center">
        <button type="submit" class="btn btn-danger my-4">Create account</button>
    </div>
</form>

@endsection
