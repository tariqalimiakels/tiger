@extends("layouts.app-auth")

@section('head_title', 'Reset Password' .' | '.config('settings.site_name') )

@section('head_url', Request::url())

@section("content")

 <form method="POST" action="{{ route('password.update') }}">
                        @csrf
<input type="hidden" name="token" value="{{ $token }}">
<div class="form-group">
    <div class="input-group input-group-alternative mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
        </div>
       
<input id="email" type="email" class="form-control" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
    </div>
</div>

<div class="form-group">
    <div class="input-group input-group-alternative">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
        </div>
        <input class="form-control" placeholder="Password" type="password" name="password" required autocomplete="new-password">
    </div>
</div>
<div class="form-group">
    <div class="input-group input-group-alternative">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
        </div>
        <input class="form-control" placeholder="Confirm Password" type="password" name="password_confirmation"
            required autocomplete="new-password">
    </div>
</div>
<div class="text-center">
    <button type="submit" class="btn btn-danger my-4">Reset Password</button>
</div>
</form>

@endsection
