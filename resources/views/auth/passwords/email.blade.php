@extends("layouts.app-auth")

@section('head_title', 'Forgot Password' .' | '.config('settings.site_name') )

@section('head_url', Request::url())

@section("content")
<div class="text-center text-muted mb-4">
    <h4>Reset password</h4>
</div>
<form method="POST" action="{{ route('password.email') }}">
    @csrf

    <div class="form-group">
        <div class="input-group input-group-alternative mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="ni ni-email-83"></i></span>
            </div>
            <input class="form-control" placeholder="Email" type="email" name="email" value="{{ old('email') }}"
                required>
        </div>
    </div>

    <div class="text-center">
        <button type="submit" class="btn btn-danger my-4">Send Password Reset Link</button>
    </div>
</form>

@endsection
