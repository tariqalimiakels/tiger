<!DOCTYPE html>
<!--
=========================================================
* Argon Design System - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system
* Copyright 2020 Creative Tim (http://www.creative-tim.com)

Coded by www.creative-tim.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<html lang="en">

<head>
    @include("_particles.head-meta")
</head>

<body class="">

	<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
		{{ csrf_field() }}
	</form>

    @if(Session::has('is_item_added') && Session::get('is_item_added') == true)
    <input name="is_item_added" type="hidden" value="">
    @endif

    @include("_particles.nav")

    <div class="wrapper">

        @yield("content")

        @include("_particles.side-cart")

        @include("_particles.footer")

    </div>

    @include("_particles.footer-meta")

</body>

</html>