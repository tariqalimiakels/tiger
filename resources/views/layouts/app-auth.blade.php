<!DOCTYPE html>
<!--
=========================================================
* Argon Design System - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system
* Copyright 2020 Creative Tim (http://www.creative-tim.com)

Coded by www.creative-tim.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<html lang="en">

<head>
    @include("_particles.head-meta")
</head>

<body class="bg-default">

    <div class="main-content">
        <nav class="navbar navbar-top navbar-horizontal navbar-expand-md bg-white navbar-dark">
            <div class="container-fluid px-7">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img height="40px" src="{{ URL::asset('client_assets/images/default-logo.png') }}"></a>
            </div>
        </nav>
        <div class="header y py-5 py-lg-8">
            <div class="container">
                <div class="header-body text-center mb-1">
                    <div class="row justify-content-center">
                        <a class="navbar-brand" href="{{ url('/') }}">

                            <img src="{{ URL::asset('client_assets/images/default-logo.png') }}" width="300"
                                class="thumbnail" alt="..."></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container mt--8 pb-5">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7">

                    <div class="card bg-secondary shadow border-0">
                        <div class="card-body px-lg-5 py-lg-5">

                            <div class="message">
                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                            </div>

                            @if(Session::has('flash_message'))
                            <div class="alert alert-success">
                                {{ Session::get('flash_message') }}
                            </div>
                            @endif

                            @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                            @endif

                            @yield("content")

                        </div>
                    </div>
                    @if(!Request::is('profile'))
                    <div class="row mt-3">
                        <div class="col-6">
                            <a href="{{ route('password.request') }}" class="text-light">
                                <small>Forgot your password?</small>
                            </a>
                        </div>
                        <div class="col-6 text-right">
                            @if(Request::is('login'))
                            <a href="{{ url('/register') }}" class="text-light">
                                <small>Create new account</small>
                            </a>
                            @else
                            <a href="{{ url('/login') }}" class="text-light">
                                <small>Already have an account?</small>
                            </a>
                            @endif
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>

    </div>

    @include("_particles.footer-meta")

</body>

</html>
