<div id="cartSideNav" class="sidenav-cart">
    <div class="offcanvas-menu-inner">
        <div class="addMenu">
            <div class="single-menu">
                <h5 class="m-0">Shopping Cart</h5>
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            </div>
            
        </div>
        <div class="minicart-content">
            <div class="searchable-container">
                @include("_particles.cart-items", ['cartListClass' => '', 'isCheckoutBtnShow' => true])
            </div>
        </div>
    </div>
</div>
