<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>@yield('head_title', config('settings.site_name') .' - '. config('settings.site_title'))</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="@yield('head_description', config('settings.site_description'))" />
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<meta property="og:type" content="article" />
<meta property="og:title" content="@yield('head_title',  config('settings.site_name'))" />
<meta property="og:description" content="@yield('head_description', config('settings.site_description'))" />
<meta property="og:image" content="@yield('head_image', url('/upload/logo.png'))" />
<meta property="og:url" content="@yield('head_url', url('/'))" />
<!-- Favicons-->
<link rel="shortcut icon" href="{{ URL::asset('upload/'.config('settings.site_favicon')) }}" type="image/x-icon">
<!--MAIN STYLE-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet"><!-- Nucleo Icons -->
<link href="{{ URL::asset('client_assets/css/css-select2.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('client_assets/css/css-nucleo-icons.css') }}" rel="stylesheet">
<link href="{{ URL::asset('client_assets/css/css-nucleo-svg.css') }}" rel="stylesheet"><!-- Font Awesome Icons -->
<link href="{{ URL::asset('client_assets/css/css-font-awesome.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link href="{{ URL::asset('client_assets/css/css-nucleo-svg.css') }}" rel="stylesheet"><!-- CSS Files -->
<link href="{{ URL::asset('client_assets/css/css-argon-design-system.css') }}" rel="stylesheet">
<link href="{{ URL::asset('client_assets/css/css-custom.css') }}" rel="stylesheet">
<link href="{{ URL::asset('client_assets/css/css-style.css') }}" rel="stylesheet">

<script src="{{ URL::asset('client_assets/js/core-jquery.min.js') }}" type="text/javascript"></script>
<script>var api_url = '{{ config('app.url') }}';</script>

{!!config('settings.site_header_code')!!}
