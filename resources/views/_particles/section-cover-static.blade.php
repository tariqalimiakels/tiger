<section class="section-profile-cover section-shaped grayscale-05 d-none d-md-none d-lg-block d-lx-block">
    <img class="bg-image" src="{{ URL::asset($cover_photo) }}" style="width: 100%;">
</section>
<section class="section py-3 restuarant-title">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="title">
                    <div class="d-align-center">
                        <h1>{!! $title !!}</h1>
                        <span class="ml-3">{!! $description !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>