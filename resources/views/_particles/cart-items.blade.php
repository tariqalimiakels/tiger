<div id="cartList" class="{{ $cartListClass }}">
    @foreach(\App\Cart::getCart(Auth::id()) as $cart_item)
    <div class="items">
        <div class="info-block block-info clearfix">
            <div class="square-box">
                <img src="{{ URL::asset('upload/menu/'.$cart_item->menu_image.'-b.jpg') }}" class="productImage"
                    width="100" height="105" alt="{{ $cart_item->item_name }}">
            </div>
            <div class="cartDesc">
                <h6>{{ $cart_item->item_name }}
                    ({{ $cart_item->restaurant_name }})</h6>
                <p>{{ $cart_item->quantity }} x
                    {{ Helper::getFormattedPrice($cart_item->item_price) }}</p>
                <ul class="pagination">
                    <li class="page-item">
                        <a href="{{URL::to('add_item/'.$cart_item->item_id.'/remove')}}" class="page-link"><i
                                class="ni ni ni-fat-delete"></i></a>
                    </li>
                    <li class="page-item">
                        <a href="{{URL::to('add_item/'.$cart_item->item_id)}}" class="page-link"><i
                                class="ni ni-fat-add"></i></a>
                    </li>
                    <li class="page-item">
                        <a href="{{URL::to('delete_item/'.$cart_item->id)}}" class="page-link"><i
                                class="fa fa-trash"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @endforeach
</div>

@if ($userId = Auth::id()) @endif
@if ($subTotal = DB::table('cart')->where('user_id', Auth::id())->sum('item_price')) @endif
@if ($deliveryCharges = \App\Cart::getDeliveryCharges(Auth::id())) @endif
<div id="totalPrices">
    <div class="totalCalc">
        @if($subTotal > 0)
        <table class="w-100">
            <thead>
                <tr>
                    <th>Sub Total:</th>
                    <th class="text-right">{{Helper::getFormattedPrice($subTotal)}}</th>
                </tr>
                @foreach(\App\Cart::getCartUniqueRestaurants(Auth::id()) as $restaurant)
                <tr>
                    <th>Delivery Charges ({{ $restaurant->restaurant_name }}):</th>
                    <th class="text-right">{{Helper::getFormattedPrice($restaurant->delivery_charges_custom)}}</th>
                </tr>
                @endforeach
                <tr>
                    <th>Total Delivery Charges:</th>
                    <th class="text-right">{{Helper::getFormattedPrice($deliveryCharges)}}</th>
                </tr>
                <tr>
                    <th>Total:</th>
                    <th class="text-right">{{Helper::getFormattedPrice($subTotal + $deliveryCharges)}}</th>
                </tr>
            </thead>
        </table>
        @else
        <span>Cart is empty!</span>
        @endif
    </div>
    @if($isCheckoutBtnShow && $subTotal > 0)
    <div>
        <a href="{{ URL::to('cart') }}" class="btn btn-primary text-white br-0">Checkout</a>
    </div>
    @endif
</div>
