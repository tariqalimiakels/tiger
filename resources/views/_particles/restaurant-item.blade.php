<div class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
    <div class="strip">
        <figure><a href="{{ URL::to('restaurant/'.$restaurant->restaurant_slug) }}"><img
                    src="{{ URL::asset('upload/restaurants/'.$restaurant->restaurant_logo.'-b.jpg') }}"
                    class="img-fluid lazy" alt=""></a>
        </figure><span class="res_title"><b><a
                    href="{{ URL::to('restaurant/'.$restaurant->restaurant_slug) }}">{{  Helper::limitCharacters($restaurant->restaurant_name) }}</a></b></span><br><span
            class="res_description">{{ Helper::limitCharacters($restaurant->category_names) }}</span><br>
            <div class="d-between">
                <span class="res_mimimum">Min order: {{ Helper::getFormattedPrice($restaurant->min_order) }}</span>
                <span class="res_mimimum">Delivery {{ $restaurant->delivery_starts_from  == null ? Helper::getFormattedPrice($restaurant->delivery_charge) : ' from ' . Helper::getFormattedPrice($restaurant->delivery_starts_from) }}</span>
            </div>
            <div class="d-between">
                {!! Helper::restaurantTodaysTimings($restaurant) !!}
            </div>

    </div>
</div>
