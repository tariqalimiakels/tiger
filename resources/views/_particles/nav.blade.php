<!-- Navbar -->
<nav id="navbar-main" class="navbar navbar-light navbar-expand-lg fixed-top">
    <div class="container-fluid">
        <a class="navbar-brand mr-lg-5" href="{{ url('/') }}">
            <img src="{{ URL::asset('client_assets/images/default-logo.png') }}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global"
            aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="navbar_global">
            <div class="navbar-collapse-header">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="#">
                            <img src="{{ URL::asset('client_assets/images/default-logo.png') }}"></a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global"
                            aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <ul class="navbar-nav align-items-lg-center ml-lg-auto">
                <li class="nav-item">
                    <div class="deliveryAddress">
                        <p>
                            <i class="fa fa-map-marker"></i>
                            Location: <span>{{ Helper::getLocationName() }}</span>
                        </p>
                    </div>
                    <div class="locationDrop">
                        {!! Form::open(array('url' => 'restaurant/search','class'=>'','id'=>'searchrestaurantnav','role'=>'form')) !!}
                        <input type="hidden" name="header_search" value="1">
                        <div class="row bannerS m-0">
                            <div class="col-9 p-0">
                                <div class="form-group m-0">
                                    <div class="input-group m-0">
                                        <div class="input-group-prepend">
                                            <select name="location" class="form-control select2">
                                                <option value="">Delivery location</option>
                                                @foreach(\App\Location::getDeliveryLocationsBelongsToRestaurants() as $location)
                                                <option value="{{ $location->id }}"
                                                    {{ session('location') == $location->id ? "selected" : '' }}>
                                                    {{ ucwords(strtolower($location->location)) }}</option>
                                                @endforeach
                                            </select>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-3 p-0">
                                @if(Request::is('restaurant/*'))
                                <button type="button" class="btn btn-danger btn-block br-0 check-now"><i class="fa fa-arrow-right"></i></button>
                                @else
                                <button type="submit" class="btn btn-danger btn-block br-0"><i class="fa fa-arrow-right"></i></button>
                                @endif
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-link-icon" href="{{ url('/') }}" target="_blank" data-toggle="tooltip"
                        title="Like us on Facebook">
                        <i class="fa fa-facebook-square"></i>
                        <span class="nav-link-inner--text d-lg-none">Facebook</span>
                    </a>
                </li>
                <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
                    <li class="nav-item dropdown">
                        @if(Auth::check())
                            <a href="#" class="btn btn-neutral btn-icon web-menu" data-toggle="dropdown" role="button">
                                <span class="btn-inner--icon">
                                    <i class="fa fa-user mr-2"></i>
                                </span>
                                <span class="nav-link-inner--text">{{ Auth::user()->name }}</span>
                            </a>
                            <div class="dropdown-menu">
                                @if(Auth::check() and Auth::user()->usertype=='User')
                                <a href="{{ url('/') }}" class="dropdown-item">Visit Site</a>
                                <a href="{{ url('/profile') }}" class="dropdown-item">Profile</a>
                                <a href="{{ url('/orders') }}" class="dropdown-item">My Orders</a>
                                @else
                                <a href="{{ url('/admin/dashboard') }}" class="dropdown-item">Dashboard</a>
                                @endif
                                <a href="javascript:void(0)" class="dropdown-item" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                    <span>Logout</span>
                                </a>
                            </div>
                            <a href="javascript:void(0)" class="nav-link nav-link-icon mobile-menu" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"> 
                                <span class="btn-inner--icon">
                                    <i class="fa fa-user mr-2"></i>
                                </span>
                                <span class="nav-link-inner--text">Logout</span>
                            </a>
                        @else
                            <a href="{{ url('/login') }}" class="btn btn-neutral btn-icon web-menu">
                                <span class="btn-inner--icon">
                                    <i class="fa fa-user mr-2"></i>
                                </span>
                                <span class="nav-link-inner--text">Login</span>
                            </a>
                            <a href="{{ url('/login') }}" class="nav-link nav-link-icon mobile-menu">
                                <span class="btn-inner--icon">
                                    <i class="fa fa-user mr-2"></i>
                                </span>
                                <span class="nav-link-inner--text">Login</span>
                            </a>
                        @endif
                    </li>
                    <li class="web-menu">

                        <a id="desCartLink" onclick="openNav()" class="btn btn-neutral btn-icon btn-cart"
                            style="cursor:pointer;" href="javascript:void(0)">
                            <span class="btn-inner--icon">
                                <i class="fa fa-shopping-cart"></i>
                            </span>
                            <span class="nav-link-inner--text">Cart</span>
                        </a>

                    </li>
                    <li class="mobile-menu">
                        <a id="mobileCartLink" onclick="openNav()" class="nav-link" style="cursor:pointer;"
                            href="javascript:void(0)">
                            <i class="fa fa-shopping-cart"></i>
                            <span class="nav-link-inner--text">Cart</span>
                        </a>

                    </li>
                </ul>
            </ul>
        </div>
    </div>
</nav><!-- End Navbar -->
