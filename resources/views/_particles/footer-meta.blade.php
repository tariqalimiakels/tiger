<!--   Core JS Files   -->
<script src="{{ URL::asset('client_assets/js/core-popper.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('client_assets/js/core-bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('client_assets/js/plugins-perfect-scrollbar.jquery.min.js') }}"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="{{ URL::asset('client_assets/js/plugins-bootstrap-switch.js') }}"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{ URL::asset('client_assets/js/plugins-nouislider.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('client_assets/js/plugins-moment.min.js') }}"></script>
<script src="{{ URL::asset('client_assets/js/plugins-datetimepicker.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('client_assets/js/plugins-bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('client_assets/js/js-select2.min.js') }}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<!-- Control Center for Argon UI Kit: parallax effects, scripts for the example pages etc -->
<script src="{{ URL::asset('client_assets/js/js-argon-design-system.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('client_assets/js/js-cartSideMenu.js') }}"></script><!-- Cart side menu -->
<script src="{{ URL::asset('client_assets/js/js-custom.js') }}"></script><!-- Custom JS -->
<script src="{{ URL::asset('client_assets/js/js-notify.js') }}"></script>
<script src="{{ URL::asset('client_assets/js/js-notify.min.js') }}"></script>
