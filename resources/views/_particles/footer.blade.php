<footer class="footer">
  <div class="container">
    <div class="row align-items-center justify-content-md-between">
      <div class="col-md-6 text-md-left text-center">
        <div class="copyright">
        &copy; 2020 <a href="{{ url('/') }}" target="_blank">{{ config('settings.site_name') }}</a>.
        </div>
      </div>
      <div class="col-md-6">
        <ul id="footer-pages" class="nav nav-footer justify-content-center justify-content-md-end">
          <li class="nav-item">
            <a href="{{ url('/about') }}" class="nav-link">About Us</a>
          </li>
          <li class="nav-item">
            <a href="{{ url('/contact') }}" class="nav-link">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>