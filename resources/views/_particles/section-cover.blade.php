<section class="section-profile-cover section-shaped grayscale-05">
    <img class="bg-image" src="{{ URL::asset($cover_photo) }}" style="width: 100%;">
</section>
<section class="section py-3 restuarant-title">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="title">
                    <div class="d-align-center">
                        <h1>{!! $title !!}</h1>
                        @if($is_category_show)
                        <span class="d-block ml-md-3 restaurant-time">{!! Helper::restaurantTodaysTimings($restaurant) !!}</span>
                        @endif                          
                        <span class="ml-md-3">{!! $description !!}</span>
                    </div>
                    @if($is_category_show)
                    {{-- <div>
                        <ul>
                            @foreach($categories as $category)
                            <li><a href="javascript: void(0);">{{ $category->category_name }}</a></li>
                            @endforeach
                        </ul>
                    </div> --}}
                    @endif
                </div>
                @if($is_type_show)
                <div class="foodTypeSlider">
                    <ul>
                        @foreach(\App\Types::orderBy('type')->get() as $type)
                        <li>
                            <div class="strip">
                                <figure><a href="{{URL::to('restaurant/type/'.$type->type_slug)}}">
                                    <img src="{{ URL::asset('upload/type/'.$type->type_image.'.jpg') }}" class="img-fluid lazy"></a>
                                </figure>
                                <span class="res_title">
                                    <b><a href="{{URL::to('restaurant/type/'.$type->type_slug)}}">{{$type->type}}</a></b>
                                </span>

                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
        </div>
    </div>
</section>