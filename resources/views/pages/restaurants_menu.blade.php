@extends("layouts.app")

@section('head_title', $restaurant->restaurant_name .' menu delivery' .' | '.config('settings.site_name') )
@section('head_url', Request::url())

@section("content")

@include("_particles.section-cover", ['title' => $title, 'description' =>
$restaurant->restaurant_address, 'cover_photo' => 'upload/restaurants/'.$restaurant->restaurant_bg.'-b.jpg', 'is_category_show' => true, 'is_type_show' => false, 'is_delivery_locations_show' => true, 'restaurant' => $restaurant])

<input type="hidden" name="restaurantID" id="restaurantID" value="{{ $restaurant->id }}">

@if ($subTotal = DB::table('cart')->where('user_id', Auth::id())->sum('item_price')) @endif

<div class="modal fade" id="productModal" z-index="999" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered addMenu modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Specify your dish</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-9 col-md-8">
                        <input id="modalID" type="hidden">
                        <div class="single-menu">
                            <div class="menuTitle">
                                <img id="modalImg" src="images/foodtiger.site-restorant">  
                                <div class="menuDesc">
                                    <p id="modalTitle" class="res_title"></p>
                                    <p id="modalDescription"></p>
                                    <p class="mp">1 x <span class="modalPrice strong"></span></p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div id="addToCart1" class="bg-light p-3">
                            @if(session('location'))
                                @if(Auth::check())
                                    <a href="" class="btn btn-outline-secondary ptom btn-block br-0 add-to-cart"><span>Add To Cart</span></a>
                                @else
                                    <a href="{{URL::to('login')}}" class="btn btn-outline-secondary ptom btn-block br-0"><span>Add To Cart</span></a>
                                @endif
                            @else
                                <a href="javascript:void(0)" class="btn btn-outline-secondary ptom btn-block br-0 check-location"><span>Add To Cart</span></a>
                            @endif
                            <div class="itemsPrice">
                                <p>Order subtotal</p>
                                <h3>{{Helper::getFormattedPrice($subTotal)}}</h3>
                                <p>Your cart contains <span class="number">{{ DB::table('cart')->where('user_id', Auth::id())->count() }}</span> items</p>
                            </div>
                            <a href="{{ URL::to('cart') }}" class="btn btn-primary btn-block br-0 mb-1">Checkout</a>
                            <a href="#" class="btn btn-primary btn-block br-0" data-dismiss="modal" aria-label="Close">Continue Shopping</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="restaurantModal" z-index="9999" tabindex="-1" role="dialog" aria-labelledby="modal-form"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 id="modalTitle" class="modal-title">{{ $restaurant->restaurant_name }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0">
                <div class="card bg-secondary shadow border-0">
                    <div class="card-body px-lg-5 py-lg-5">
                        
                        <div class="row">
                            <div class="col-sm col-md col-lg col-lg text-center">
                            <img id="modalImg" src="{{ URL::asset('upload/restaurants/'.$restaurant->restaurant_logo.'-b.jpg') }}" width="295px" height="200px" alt="{{ $restaurant->restaurant_name }}">
                            </div>
                            <div class="col-sm col-md col-lg col-lg">
                                <h5>Delivery hours</h5>
                                <ul>
                                    @foreach($weekly as $day => $dayColumns)
                                    @if($start = $dayColumns[0])@endif
                                    @if($end = $dayColumns[1])@endif
                                    @if($closed = ($restaurant->$start == "" || $restaurant->$end == "") ? true : false)@endif
                                    <li>{{ $day }} - {!! !$closed ? date("h:i A", strtotime($restaurant->$start))  .' - '. date("h:i A", strtotime($restaurant->$end)) : '<span class="closed-text">Closed</span>' !!}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@php
$categories = \App\Categories::where('restaurant_id',$restaurant->id)->where('status', 1)->orderBy('category_name')->get();
@endphp

<section class="section pt-0">
    <div class="container container-restorant resturantMenu">
        @error('message')
        <div class="message">
            <div class="alert alert-danger">
                <p class="alert-message">{{ $message }}</p>
            </div>
        </div>
        @enderror
        <div class="row">
            <div class="col-lg-3 col-md-4 stickyParent d-none d-md-block">
                <div id="sticky-header">
                    @foreach($categories as $n=>$cat)
                        @if(\App\Menu::where('menu_cat',$cat->id)->orderBy('menu_name')->count() > 0)
                        <div class="cat-types">
                            <a href="javascript: void(0);">{{Helper::limitCharacters($cat->category_name, 25)}}</a>
                        </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="col-lg-9 col-md-8">
                @foreach($categories as $n=>$cat)
                    @if(\App\Menu::where('menu_cat',$cat->id)->orderBy('menu_name')->count() > 0)
                    <div class="menuType">
                        <div class="col-12 p-0">
                            <div class="propBanner">
                                <h2>{{ Helper::limitCharacters($cat->category_name, 40)}} {!! Helper::categoryTodaysTimings($cat) !!}</h2>
                                <button class="btn btn-white btn-accordion d-block d-md-none"><i class="fa fa-angle-down"></i></button>
                            </div>
                        </div>
                        <div class="col-12 mobileAccordion">
                            <div class="row">
                            @foreach(\App\Menu::where('menu_cat',$cat->id)->orderBy('menu_name')->get() as $menu_item)
                                <div class="col-md-6">
                                    <div class="single-menu strip m-0" data-id="{{ $menu_item->id }}" data-name="{{ $menu_item->menu_name }}"
                                        data-image="{{ URL::asset('upload/menu/'.$menu_item->menu_image.'-b.jpg') }}"
                                        data-price="{{ Helper::getFormattedPrice($menu_item->price) }}"
                                        data-description="{{ $menu_item->description }}"
                                        data-url="{{URL::to('add_item/'.$menu_item->id)}}">
                                        <div class="menuTitle">
                                            <p class="res_title">{{Helper::limitCharacters($menu_item->menu_name, 30)}}</p>
                                            <p class="res_description">{{Helper::limitCharacters($menu_item->description, 40)}}</p>
                                        </div>
                                        <div class="d-align-center mt-md-2">
                                            <p class="res_mimimum">{{ Helper::getFormattedPrice($menu_item->price) }}</p>
                                            
                                            @if(Helper::restaurantTodaysTimings($restaurant, true) && Helper::categoryTodaysTimings($cat, true))
                                            <a class="btn btn-outline-secondary btn-sm ptom menu-item">
                                                <span>Add</span>
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</section>

<script>
    var div_name = $('#sticky-header');
var div_top;
setTimeout(function() {
    div_top = div_name.offset().top;
}, 500);

function sticky_relocate() {
    var headerHeight = $('#navbar-main').outerHeight();
    var window_top = $(window).scrollTop();
    var parentOffset = $('.resturantMenu').offset().top;
    var outerHeight = $('.resturantMenu').outerHeight();
    if (window_top > div_top) {
        var div_setTop = window_top - parentOffset + headerHeight;
        div_name.addClass('stick');
        if (div_setTop < (outerHeight - headerHeight)) {
            div_name.css('top', div_setTop + 'px');
        }
    } else if (window_top < div_top) {
        div_name.removeClass('stick');
    }
}

window.onload = window.onresize = fixElements;

function fixElements() {
    var viewportWidth = document.documentElement.clientWidth;
    if (viewportWidth > 767) {
        $(window).scroll(sticky_relocate);
    }
}
</script>

@endsection
