@extends("layouts.app")

@section('head_title', config('widgets.about_title') .' | '.config('settings.site_name') )
@section('head_url', Request::url())

@section("content")

@include("_particles.section-cover-static", ['title' => 'About Us', 'description' =>
'', 'cover_photo' => 'client_assets/images/default-cover.jpg'])

<div class="what-we-do">
    <div class="container about_block">

        <div class="col-md-12">

            {!!config('widgets.about_desc')!!}

        </div>

    </div>
</div>


@endsection
