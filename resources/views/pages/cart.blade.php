@extends("layouts.app")

@section('head_title', 'Checkout' .' | '.config('settings.site_name') )

@section('head_url', Request::url())

@section("content")

@include("_particles.section-cover-static", ['title' => 'Checkout', 'description' => '', 'cover_photo' => 'client_assets/images/default-cover.jpg'])

<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                @if(Auth::check())
                    <div class="row justify-content-center">
                        <div class="col-lg-9">
                            @error('message')
                            <div class="message">
                                <div class="alert alert-danger">
                                    <p class="alert-message">{{ $message }}</p>
                                </div>
                            </div>
                            @enderror
                            <h3>Finalize order</h3>
                            <br>
                            {!! Form::open(array('url' =>
                            'order_details','class'=>'','id'=>'order_details','role'=>'form')) !!}

                            <div class="form-group">
                                <label class="form-control-label" for="city">Area</label>
                                {{-- <input type="text" name="location" value="{{ isset($location) ? $location->location : '' }}" class="form-control"> --}}
                                <select name="location" class="form-control select2"  {{ (session('location')) ? "readonly" : "" }}>
                                    <option value="">Delivery location</option>
                                    @foreach(\App\Location::orderBy('location', 'asc')->get() as $location)
                                    <option value="{{ $location->id }}" {{ (session('location') && session('location') == $location->id) ? "selected" : '' }}>{{ $location->location }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="mobile">Phone</label>
                                <input name="mobile" id="mobile" type="text" class="form-control"
                            placeholder="Your phone here" value="{{ old('mobile') }}" required>
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="address">Address</label>
                                <input name="address" id="address" type="text" class="form-control"
                                    placeholder="Your address here" value="{{ old('address') }}" required>
                            </div>

                            @foreach(\App\Cart::getCartUniqueRestaurants(Auth::id()) as $restaurant)
                            <div class="form-group">
                                <label class="form-control-label" for="comment">Comment for
                                    {{ $restaurant->restaurant_name }}</label>
                                <textarea name="comment[{{$restaurant->restaurant_id}}]" id="comment"
                                    class="form-control"
                                    placeholder="Your comment for {{ $restaurant->restaurant_name }} here"
                                    required>{{ old('comment.'.$restaurant->restaurant_id) }}</textarea>
                            </div>
                            @endforeach

                            <div class="form-group">
                                <br>
                                <h4>Payment method</h4>
                                <br>
                                <div class="custom-control custom-radio mb-3">
                                    <input name="paymentType" class="custom-control-input" id="cashOnDelivery"
                                        type="radio" value="cod" checked="">
                                    <label class="custom-control-label" for="cashOnDelivery">Cash on delivery</label>
                                </div>

                                <div class="text-center" id="totalSubmitCOD" style="display: block;">
                                    <button type="submit" class="btn btn-success mt-4" type="submit">Place
                                        order</button>
                                </div>

                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                @else
                <div class="row justify-content-center">
                    <div class="col-lg-9">

                        <div class="row d-flex justify-content-center">
                            <a class="btn btn-primary btn-md " href="{{ URL::to('/login') }}">Login</a>
                        </div>
                        <div class="text-center">
                            <br><span class="nav-link-inner--text">Login or register to finalize your order.</span>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="col-md-6">
                <h3>Your Cart<span class="font-weight-light"></span></h3>
                @include("_particles.cart-items", ['cartListClass' => 'border-top', 'isCheckoutBtnShow' => false])
            </div>
        </div>
    </div>
</section>

@endsection
