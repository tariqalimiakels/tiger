@extends("layouts.app")
@section("content")

<div class="bannerSearch">
    <section class="section-profile-cover section-shaped my-0 d-none d-md-none d-lg-block bannerImg">
        <div class="row">
            <div class="col-md-6 d-none d-md-block"></div>
            <div class="col-md-6">
                <!-- Circles background -->
                <img class="bg-image" src="{{ URL::asset('client_assets/images/default-cover.jpg') }}"
                    style="width: 100%;">
            </div>
    </section>
    <div class="banner-content">
        <div class="container">
            <h1>Food Delivery from<br><b>Pakistan&rsquo;s</b> Best Local Restaurants</h1>
            <p>The meals you love, delivered with care and COVID-19 safety</p>

            {!! Form::open(array('url' => 'restaurant/search','class'=>'','id'=>'searchrestaurant','role'=>'form')) !!}
            <div class="row bannerS">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <select name="location" class="form-control select2">
                                    <option value="">Delivery location</option>
                                    @foreach(\App\Location::getDeliveryLocationsBelongsToRestaurants() as $location)
                                    <option value="{{ $location->id }}"
                                        {{ session('location') == $location->id ? "selected" : '' }}>
                                        {{ ucwords(strtolower($location->location)) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input name="search_keyword" id="search" class="form-control lg" value=""
                                placeholder="Search for restaurant, cuisine or a dish" type="text">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-danger">Find restaurants near you</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    
</div>

<section class="section indexThumbs">
    <div class="container">
        <h1>Popular Types</h1>
        <br>
        <div class="row">

            @foreach($types as $type)
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
                <div class="strip">
                    <figure><a href="{{URL::to('restaurant/type/'.$type->type_slug)}}"><img
                                src="{{ URL::asset('upload/type/'.$type->type_image.'.jpg') }}"
                                class="img-fluid lazy" alt="{{$type->type}}"></a>
                    </figure>
                    <span class="res_title">
                        <b><a href="{{URL::to('restaurant/type/'.$type->id)}}">{{$type->type}}</a></b>
                    </span>

                </div>
            </div>
            @endforeach

        </div>
        <h1>Popular Restaurants</h1>
        <br>
        <div class="row restuarants">

            @foreach($restaurants as $i => $restaurant)
            @include("_particles.restaurant-item", [$restaurant])
            @endforeach
        </div>
        <hr>
        <div class="row row-grid align-items-center mb-md-5">
            <div class="col-lg-6">
                <h3 class="text-primary mb-2">Download the food you love</h3>
                <h6 class="mb-0 font-weight-light">It`s all at your fingertips - the restaurants you love. Find
                    the right food to suit your mood, and make the first bite last. Go ahead, download us.</h6>
            </div>
            <div class="col-lg-4 offset-lg-1 col-md-6 text-lg-center btn-wrapper">
                <div class="row">
                    <div class="col-6">
                        <a href="{{ url('/') }}" target="_blank"><img class="img-fluid"
                                src="{{ URL::asset('client_assets/images/default-playstore.png') }}" alt="..."></a>
                    </div>
                    <div class="col-6">
                        <a href="{{ url('/') }}" target="_blank"><img class="img-fluid"
                                src="{{ URL::asset('client_assets/images/default-appstore.png') }}" alt="..."></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
