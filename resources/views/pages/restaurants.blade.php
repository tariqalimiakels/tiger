@extends("layouts.app")

@section('head_title', $headTitle)
@section('head_url', Request::url())

@section("content")

@include("_particles.section-cover", ['title' => $title, 'description' => $description, 'cover_photo' => 'client_assets/images/default-cover.jpg', 'is_category_show' => false, 'is_type_show' => true, 'is_delivery_locations_show' => false])

<section class="section section-lg pt-lg-0 " style="padding-top: 0px">
    <div class="container container-restorant">
        <br><br>

        <div class="row">
            @foreach($restaurants as $restaurant)
            @include("_particles.restaurant-item", [$restaurant])
            @endforeach
        </div>
        <br>

        <div class="row">
            {{ $restaurants->links() }}
        </div>
    </div>
</section>

@endsection
