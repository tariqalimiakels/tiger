@extends("layouts.app")

@section('head_title', 'Edit Profile' .' | '.config('settings.site_name') )

@section('head_url', Request::url())

@section("content")

<div class="main-content">

    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    </div>
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">Edit Profile</h3>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="message">
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </div>
                        @if(Session::has('flash_message'))
                        <div class="alert alert-success">
                            {{ Session::get('flash_message') }}
                        </div>
                        @endif

                        {!! Form::open(array('url' => 'profile','class'=>'','id'=>'myProfile','role'=>'form')) !!}

                        <h6 class="heading-small text-muted mb-4">User information</h6>

                        <div class="pl-lg-4">
                            <div class="form-group focused">
                                <label class="form-control-label" for="name">Name</label>
                                <input class="form-control form-control-alternative" placeholder="First Name"
                                    type="text" name="name" value="{{$user->name}}" required autofocus>
                            </div>
                            
                            <div class="form-group focused">
                                <label class="form-control-label" for="email">Email</label>
                                <input class="form-control form-control-alternative" placeholder="Email" type="email"
                                    name="email" value="{{$user->email}}" required>

                            </div>
                            <div class="form-group">
                                <label class="form-control-label" for="phone">Phone</label>
                                <input class="form-control form-control-alternative" placeholder="Phone" type="phone"
                                    name="phone" value="{{$user->mobile}}" required>

                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-success mt-4">Save</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <hr class="my-4">
                        {!! Form::open(array('url' => 'change_pass','class'=>'','id'=>'myProfile','role'=>'form')) !!}

                        <h6 class="heading-small text-muted mb-4">Password</h6>

                        <div class="pl-lg-4">

                            <div class="form-group">
                                <label class="form-control-label" for="password">New Password</label>
                                <input class="form-control form-control-alternative" placeholder="Password"
                                    type="password" name="password">

                            </div>
                            <div class="form-group">
                                <label class="form-control-label" for="input-password-confirmation">Confirm New
                                    Password</label>
                                <input class="form-control form-control-alternative" placeholder="Confirm Password"
                                    type="password" name="password_confirmation">
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-success mt-4">Change password</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="row align-items-center justify-content-xl-between">

            </div>
        </footer>
    </div>
</div>

@endsection
