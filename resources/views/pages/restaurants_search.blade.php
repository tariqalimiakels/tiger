@extends("layouts.app")

@section('head_title', 'Restaurant Search' .' | '.config('settings.site_name') )

@section('head_url', Request::url())

@section("content")

@include("_particles.section-cover", ['title' => 'Restaurants', 'description' => $description, 'cover_photo' => 'client_assets/images/default-cover.jpg'])

<section class="section pt-0 ">
    <div class="container container-restorant pt-3">

        <div class="row">
            @foreach($restaurants as $restaurant)
            @include("_particles.restaurant-item", [$restaurant])
            @endforeach
        </div>

        <div class="row">
            {{ $restaurants->links() }}
        </div>
    </div>
</section>

@endsection
