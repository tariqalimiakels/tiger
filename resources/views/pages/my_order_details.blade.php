@extends("layouts.app")

@section('head_title', 'My Orders' .' | '.config('settings.site_name') )

@section('head_url', Request::url())

@section("content")

<div class="main-content">

    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    </div>
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h5 class="mb-0">#{{ $order->id }} -
                                {{ Helper::readableDateFormat($order->created_at) }}
                            </h5>
                        </div>
                    </div>
                    <div class="table-responsive">

                        <table class="table align-items-center" border="1">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">S.no</th>
                                    <th class="table-web" scope="col">Menu</th>
                                    <th class="table-web" scope="col">Item Price</th>
                                    <th class="table-web" scope="col">Quantity</th>
                                    <th class="table-web" scope="col">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($restaurants as $restaurant)
                                <tr class="bg-gradient-info">
                                    <td colspan="5" align="center">
                                        <h5 style="color:white; font-weight:bold;">{{ $restaurant->restaurant_name }} - Delivery Time ({{ $restaurant->delivery_time }})
                                        </h5>
                                    </td>
                                </tr>
                                @if ($subTotal = 0) @endif
                                @foreach(\App\Order::getOrderDetails($order->id, $restaurant->restaurant_id) as
                                $order_item)
                                <tr>
                                    <td>
                                        {{ $loop->index+1 }}
                                    </td>
                                    <td>
                                        {{ $order_item->item_name }}
                                    </td>
                                    <td>
                                        {{  Helper::getFormattedPrice($order_item->original_price) }}
                                    </td>
                                    <td>
                                        {{ $order_item->quantity }}
                                    </td>
                                    <td>
                                        {{ Helper::getFormattedPrice($order_item->item_price) }}
                                    </td>
                                </tr>
                                @endforeach
                                <tr class="user-total">
                                    <td colspan="4">Sub Total</td>
                                    <td>{{  Helper::getFormattedPrice($restaurant->sub_total) }}</td>
                                </tr>
                                <tr class="user-total">
                                    <td colspan="4">Delivery Charges</td>
                                    <td>{{  Helper::getFormattedPrice($restaurant->delivery_charges) }}</td>
                                </tr>
                                <tr class="user-total">
                                    <td colspan="4">Total ({{ $restaurant->restaurant_name }})</td>
                                    <td>{{  Helper::getFormattedPrice($restaurant->total_amount) }}</td>
                                </tr>
                                @endforeach
                                <tr class="bg-gradient-light">
                                    <td colspan="5" align="center">
                                        <h5 style="color:white; font-weight:bold;">Grand Total
                                        </h5>
                                    </td>
                                </tr>
                                <tr class="user-total">
                                    <td colspan="4">Sub Total</td>
                                    <td>{{  Helper::getFormattedPrice($order->sub_total) }}</td>
                                </tr>
                                <tr class="user-total">
                                    <td colspan="4">Total Delivery Charges</td>
                                    <td>{{  Helper::getFormattedPrice($order->delivery_charges) }}</td>
                                </tr>
                                <tr class="user-total">
                                    <td colspan="4">Total</td>
                                    <td>{{  Helper::getFormattedPrice($order->total_amount) }}</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
