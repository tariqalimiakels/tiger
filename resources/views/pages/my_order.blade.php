@extends("layouts.app")

@section('head_title', 'My Orders' .' | '.config('settings.site_name') )

@section('head_url', Request::url())

@section("content")

<div class="main-content">

    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    </div>
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <form method="GET" action="#">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">Orders</h3>
                                </div>
                                <div class="col-4 text-right">
                                    <button id="show-hide-filters"
                                        class="btn btn-icon btn-1 btn-sm btn-outline-secondary" type="button">
                                        <span class="btn-inner--icon"><i id="button-filters"
                                                class="ni ni-bold-down"></i></span>
                                    </button>
                                </div>
                            </div>
                            <br />

                        </form>
                    </div>
                    <div class="col-12">
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Order No</th>
                                    <th class="table-web" scope="col">Phone</th>
                                    <th class="table-web" scope="col">Location</th>
                                    <th class="table-web" scope="col">Address to Deliver</th>
                                    <th class="table-web" scope="col">Sub Total</th>
                                    <th class="table-web" scope="col">Delivery Charges</th>
                                    <th class="table-web" scope="col">Total</th>
                                    <th class="table-web" scope="col">Last status</th>
                                    <th class="table-web" scope="col">Created At</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($order_list as $order_item)
                                <tr>
                                    <td>
                                        <a class="btn badge badge-success badge-pill"
                                            href="{{ url('/orders-details', $order_item->id) }}">#{{ $order_item->id }}</a>
                                    </td>
                                    <td class="table-web">
                                        {{ $order_item->phone }}
                                    </td>
                                    <td class="table-web">
                                        {{ $order_item->location }}
                                    </td>
                                    <td class="table-web">
                                        {{ $order_item->address }}
                                    </td>
                                    <td class="table-web">
                                        {{ Helper::getFormattedPrice($order_item->sub_total) }}
                                    </td>
                                    <td class="table-web">
                                        {{ Helper::getFormattedPrice($order_item->delivery_charges) }}
                                    </td>
                                    <td class="table-web">
                                        {{ Helper::getFormattedPrice($order_item->total_amount) }}
                                    </td>
                                    <td class="table-web">
                                        <span class="badge badge-primary badge-pill">{{ $order_item->status }}</span>
                                    </td>
                                    <td class="table-web">
                                        {{ Helper::readableDateFormat($order_item->created_at) }}
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

@endsection
