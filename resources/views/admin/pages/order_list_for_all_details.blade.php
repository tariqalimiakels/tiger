@extends("admin.admin_app")

@section("content")
<div id="main">
    <div class="page-header">
        <h2>All Order List Details</h2>
        <a href="{{ URL::to('admin/allorder') }}" class="btn btn-default-light btn-xs"><i class="md md-backspace"></i>
            Back</a>

    </div>
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @endif

    <div class="panel panel-default panel-shadow">
        <div class="panel-body">

            <table id="order_data_table" class="table table-striped table-hover dt-responsive" cellspacing="0"
                width="100%">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">S.no</th>
                        <th class="table-web" scope="col">Menu</th>
                        <th class="table-web" scope="col">Item Price</th>
                        <th class="table-web" scope="col">Quantity</th>
                        <th class="table-web" scope="col">Total</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($restaurants as $restaurant)
                    <tr style="background:#90c652;">
                        <td colspan="5" align="center">
                            <h5 style="font-weight:bold; color:white;">{{ $restaurant->restaurant_name }}
                            </h5>
                        </td>
                    </tr>
                    @if ($subTotal = 0) @endif
                    @foreach(\App\Order::getOrderDetails($order->id, $restaurant->restaurant_id) as
                    $order_item)
                    <tr>
                        <td>
                            {{ $loop->index+1 }}
                        </td>
                        <td>
                            {{ $order_item->item_name }}
                        </td>
                        <td>
                            {{ Helper::getFormattedPrice($order_item->original_price) }}
                        </td>
                        <td>
                            {{ $order_item->quantity }}
                        </td>
                        <td>
                            {{ Helper::getFormattedPrice($order_item->item_price) }}
                        </td>
                    </tr>
                    @endforeach
                    <tr class="user-total">
                        <td colspan="4">Sub Total</td>
                        <td>{{  Helper::getFormattedPrice($restaurant->sub_total) }}</td>
                    </tr>
                    <tr class="user-total">
                        <td colspan="4">Delivery Charges</td>
                        <td>{{  Helper::getFormattedPrice($restaurant->delivery_charges) }}</td>
                    </tr>
                    <tr class="user-total">
                        <td colspan="4">Total ({{ $restaurant->restaurant_name }})</td>
                        <td>{{  Helper::getFormattedPrice($restaurant->total_amount) }}</td>
                    </tr>
                    <tr class="user-total">
                        <td colspan="4">Commission</td>
                        <td>{{  Helper::getFormattedPrice($restaurant->commission) }}</td>
                    </tr>
                    @endforeach
                    <tr style="background: #424242;">
                        <td colspan="5" align="center">
                            <h5 style="font-weight:bold; color:white;">Grand Total
                            </h5>
                        </td>
                    </tr>
                    <tr class="user-total">
                        <td colspan="4">Sub Total</td>
                        <td>{{  Helper::getFormattedPrice($order->sub_total) }}</td>
                    </tr>
                    <tr class="user-total">
                        <td colspan="4">Delivery Charges</td>
                        <td>{{  Helper::getFormattedPrice($order->delivery_charges) }}</td>
                    </tr>
                    <tr class="user-total">
                        <td colspan="4">Total</td>
                        <td>{{  Helper::getFormattedPrice($order->total_amount) }}</td>
                    </tr>
                    <tr class="user-total">
                        <td colspan="4">Commission</td>
                        <td>{{  Helper::getFormattedPrice(\App\MainRestaurantOrder::where('main_order_id', $order->id)->sum('commission')) }}</td>
                    </tr>
                </tbody>
            </table>

        </div>
        <div class="clearfix"></div>
    </div>

</div>

@endsection
