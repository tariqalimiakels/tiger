@extends("admin.admin_app")

@section("content")
<div id="main">
    
    
    <div class="page-header">
    <h2>Order List Details #{{ $order->main_order_id }}</h2>
    <a href="{{ URL::to('admin/orderlist') }}" class="btn btn-default-light btn-xs"><i class="md md-backspace"></i> Back</a>
    </div>

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @endif

    <div class="panel panel-default panel-shadow">
        <div class="panel-body">

            <table id="" class="table table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.no</th>
                        <th>Item</th>
                        <th>Item Price</th>
                        <th>Quantity</th>
                        <th>Total Price</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($order_list as $orderList)
                    <tr>
                        <td>{{ $loop->index+1 }}</td>
                        <td>{{ $orderList->item_name }}</td>
                        <td>{{ Helper::getFormattedPrice($orderList->original_price) }}</td>
                        <td>{{ $orderList->quantity }}</td>
                        <td>{{ Helper::getFormattedPrice($orderList->item_price) }}</td>
                    </tr>
                    @endforeach

                </tbody>
            </table>

        </div>
        <div class="clearfix"></div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-md-6">
            <div class="panel panel-default panel-shadow">

                <div class="panel-body">
                <div class="card">
                    <div class="card-header">
                      <h4>User Info</h4>
                    </div>
                    <ul class="list-group list-group-flush">
                    <li class="list-group-item"><strong>Name:</strong> {{ $order->name }}</li>
                    <li class="list-group-item"><strong>Area:</strong> {{ $order->location }}</li>
                    <li class="list-group-item"><strong>Address:</strong> {{ $order->address }}</li>
                    <li class="list-group-item"><strong>Phone:</strong> {{ $order->phone }}</li>
                    </ul>
                  </div>
                </div>
            </div>
            
        </div>
        <div class="col-sm-6 col-md-6">
            <div class="panel panel-default panel-shadow">

                <div class="panel-body">
                <div class="card">
                    <div class="card-header">
                      <h4>Payment/Other Info</h4>
                    </div>
                    @if($totalPrice = DB::table('restaurant_order')->where('restaurant_id', $restaurantId)->where('main_order_id', $order->id)->sum('item_price'))@endif
                    <ul class="list-group list-group-flush">
                <li class="list-group-item"><strong>Sub Total:</strong> {{ Helper::getFormattedPrice($order->sub_total) }}</li>
                        <li class="list-group-item"><strong>Delivery Charges:</strong> {{ Helper::getFormattedPrice($order->delivery_charges) }}</li>
                        <li class="list-group-item"><strong>Total:</strong> {{ Helper::getFormattedPrice($order->total_amount) }}</li>
                        <form method="POST" action="{{ url('/admin/orderlist/details') }}">
                        <input type="hidden" name="main_restaurant_order_id" value="{{ $order->id }}">
                        @csrf
                        <li class="list-group-item"><strong>Paid Amount (Excluded Delivery Charges):</strong> <input type="number" name="paid_amount" value="{{ $order->paid_amount > 0 ? $order->paid_amount : $order->sub_total }}"><input type="submit" value="Update"></li>
                        </form>
                    </ul>
                  </div>
                </div>
            </div>
            
        </div>
    </div>

</div>

@endsection
