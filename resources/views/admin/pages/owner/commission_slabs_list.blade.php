@extends("admin.admin_app")

@section("content")
<div id="main">
    <div class="page-header">
        <h2>Commissions Slab</h2>
    </div>

    <div class="panel panel-default panel-shadow">
        <div class="panel-body">

            <table class="table table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Min Price</th>
                        <th>Max Price</th>
                        <th>Commission</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($commissionSlabs as $i => $slab)
                    <tr>
                        <td>{{ Helper::getFormattedPrice($slab->min_value) }}</td>
                        <td>{{ Helper::getFormattedPrice($slab->max_value) }}</td>
                        <td>{{ Helper::getFormattedPrice($slab->commission) }}</td>
                    </tr>
                    @endforeach

                </tbody>
            </table>

        </div>
        <div class="clearfix"></div>
    </div>

</div>

@endsection
