@extends("admin.admin_app")

@section("content")

<div id="main">
    <div class="page-header">
        <h2> {{ isset($restaurant->restaurant_name) ? 'Edit: '. $restaurant->restaurant_name : 'Add Restaurant' }}</h2>

        <a href="{{ URL::to('admin/restaurants') }}" class="btn btn-default-light btn-xs"><i
                class="md md-backspace"></i> Back</a>

    </div>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @endif

    <div class="panel panel-default">
        <div class="panel-body">
            {!! Form::open(array('url' => array('admin/restaurants/addrestaurant'),'class'=>'form-horizontal
            padding-15','name'=>'category_form','id'=>'category_form','role'=>'form','enctype' =>
            'multipart/form-data')) !!}
            <input type="hidden" name="id" value="{{ isset($restaurant->id) ? $restaurant->id : null }}">
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Restaurant Type</label>
                <div class="col-sm-9">
                    <select id="basic" name="restaurant_type" class="selectpicker show-tick form-control">
                        <option value="">Select Type</option>

                        @foreach($types as $type)
                        <option value="{{$type->id}}"
                            {{ isset($restaurant->restaurant_type) && $restaurant->restaurant_type==$type->id ? 'selected' : old('restaurant_type') == $type->id ? 'selected' : '' }}>
                            {{$type->type}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Restaurant Name</label>
                <div class="col-sm-9">
                    <input type="text" name="restaurant_name"
                        value="{{ isset($restaurant->restaurant_name) ? $restaurant->restaurant_name : old('restaurant_name') }}"
                        class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Commission Percentage(%)</label>
                <div class="col-sm-9">
                    <input type="text" name="commission_percentage"
                        value="{{ isset($restaurant->commission_percentage) ? $restaurant->commission_percentage : config('constants.commission_percentage') }}"
                        class="form-control" required disabled>
                </div>
            </div>

            {{-- <div class="form-group">
                <label for="" class="col-sm-3 control-label">Restaurant Slug</label>
                <div class="col-sm-9">
                    <input type="text" name="restaurant_slug"
                        value="{{ isset($restaurant->restaurant_slug) ? $restaurant->restaurant_slug : null }}"
            class="form-control">
        </div>
    </div> --}}
    <div class="form-group">
        <label for="" class="col-sm-3 control-label">Minimum Order</label>
        <div class="col-sm-9">
            <input type="number" name="min_order"
                value="{{ isset($restaurant->min_order) ? $restaurant->min_order : old('min_order') }}"
                class="form-control" required>
        </div>
    </div>
    <div class="form-group">
        <label for="" class="col-sm-3 control-label">Maximum Order</label>
        <div class="col-sm-9">
            <input type="number" name="max_order"
                value="{{ isset($restaurant->max_order) ? $restaurant->max_order : old('max_order') }}"
                class="form-control" required>
        </div>
    </div>
    <div class="form-group">
        <label for="" class="col-sm-3 control-label">Address</label>
        <div class="col-sm-9">
            <textarea name="restaurant_address" id="restaurant_address" cols="60" rows="3" class="form-control"
                required>{{ isset($restaurant->restaurant_address) ? $restaurant->restaurant_address : old('restaurant_address') }}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="" class="col-sm-3 control-label">Delivery Areas/Locations</label>
        <div class="col-sm-9">
            <select class="js-example-basic-single form-control" name="locations[]" multiple="multiple">
                @foreach($locations as $location)
                <option value="{{ $location->id }}" {{in_array($location->id, $location_deliveries) ? 'selected':''}}>
                    {{ $location->location }}
                </option>
                @endforeach
            </select>
        </div>
    </div>


    @foreach($locations as $location)
    @if(in_array($location->id, $location_deliveries))
    <div class="form-group">
        <label for="" class="col-sm-3 control-label">{{ $location->location }} (DC) & Delivery Time</label>
        <div class="input-group col-sm-3">
            <div class="input-group-prepend">
                <input type="number" name="delivery_charges[{{ $location->id }}]"
                    value="{{ isset($location_delivery_charges[$location->id]) ? $location_delivery_charges[$location->id] : old('delivery_charges'.$location->id) }}"
                    class="form-control">
            </div>
            <select class="form-control" name="delivery_times[{{ $location->id }}]">
                @foreach(Helper::timeDuration() as $duration)
                <option value="{{ $duration  }}"
                    {{ isset($location_delivery_times[$location->id]) && $location_delivery_times[$location->id]==$duration ? 'selected' : old('delivery_times'.$location->id) == $duration ? 'selected' : '' }}>
                    {{ $duration }}</option>
                @endforeach
            </select>
        </div>
    </div>
    @endif
    @endforeach


    <div class="form-group" style="margin-top: 20px;">
        <label for="" class="col-sm-3 control-label">Default Delivery Charges</label>
        <div class="col-sm-9">
            <input type="number" name="delivery_charge"
                value="{{ isset($restaurant->delivery_charge) ? $restaurant->delivery_charge : old('delivery_charge') }}"
                class="form-control" required>
            <small>Default DC will be used when no DC found for respective location. </small>
        </div>
    </div>

    <div class="form-group" style="margin-top: 20px;">
        <label for="" class="col-sm-3 control-label">Default Delivery Time</label>
        <div class="col-sm-9">
            <select class="form-control" name="delivery_time">
                @foreach(Helper::timeDuration() as $duration)
                <option value="{{ $duration  }}"
                    {{ isset($restaurant->delivery_time) && $restaurant->delivery_time==$duration ? 'selected' : old('delivery_time') == $duration ? 'selected' : '' }}>
                    {{ $duration }}</option>
                @endforeach
            </select>
            <small>Default Delivery Time (DT) will be used when no DT found for respective location. </small>
        </div>
    </div>

    {{-- <div class="form-group">
                <label for="" class="col-sm-3 control-label">Things you offer</label>
                <div class="col-sm-9">
                    <input type="text" name="restaurant_description"
                        value="{{ isset($restaurant->restaurant_description) ? $restaurant->restaurant_description : null }}"
    class="form-control">
    <small>BBQ, Snacks, Pizza, Fast Food, Fried chicken</small>
</div>
</div> --}}
    <input type="hidden" name="restaurant_description" value="" class="form-control">

    <div class="form-group">
        <label for="avatar" class="col-sm-3 control-label">Restaurant Logo</label>
        <div class="col-sm-9">
            <div class="media">
                <div class="media-left">
                    @if(isset($restaurant->restaurant_logo))

                    <img src="{{ URL::asset('upload/restaurants/'.$restaurant->restaurant_logo.'-s.jpg') }}" width="100"
                        alt="person">
                    @endif

                </div>
                <div class="media-body media-middle">
                    <input type="file" name="restaurant_logo" class="filestyle" {{ !$isEditMode ? 'required' : '' }}>
                </div>
            </div>

        </div>
    </div>

    <div class="form-group">
        <label for="avatar" class="col-sm-3 control-label">Restaurant Cover Photo</label>
        <div class="col-sm-9">
            <div class="media">
                <div class="media-left">
                    @if(isset($restaurant->restaurant_bg))

                    <img src="{{ URL::asset('upload/restaurants/'.$restaurant->restaurant_bg.'-s.jpg') }}" width="100"
                        alt="person">
                    @endif

                </div>
                <div class="media-body media-middle">
                    <input type="file" name="restaurant_bg" class="filestyle" {{ !$isEditMode ? 'required' : '' }}>
                </div>
            </div>

        </div>
    </div>

    <h4>Opening and closing time</h4>

    @foreach($weekly as $key => $val)
    <div class="form-group">
        <label for="" class="col-sm-3 control-label">{{ $key }}</label>
        @foreach($val as $daily_open_close)
        <div class="col-sm-3">
            <input type="time" name="{{ $daily_open_close }}"
                value="{{ (isset($restaurant->$daily_open_close) && $restaurant->$daily_open_close != "") ? $restaurant->$daily_open_close : old($daily_open_close) }}"
                class="form-control">
        </div>
        @endforeach
    </div>
    @endforeach

    <hr>
    <div class="form-group">
        <div class="col-md-offset-3 col-sm-9 ">
            <button type="submit"
                class="btn btn-primary">{{ isset($restaurant->id) ? 'Edit Restaurant ' : 'Add Restaurant' }}</button>
        </div>
    </div>

    {!! Form::close() !!}
    </div>
</div>


</div>

@endsection
