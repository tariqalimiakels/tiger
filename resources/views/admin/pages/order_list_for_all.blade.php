@extends("admin.admin_app")

@section("content")
<div id="main">
    <div class="page-header">
        <h2>All Order List</h2>
    </div>
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @endif

    <div class="panel panel-default panel-shadow">
        <div class="panel-body">

            <table id="order_data_table" class="table table-striped table-hover dt-responsive" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        <th>Order No</th>
                        <th>User</th>
                        <th>Mobile</th>
                        <th>Area</th>
                        <th>Address</th>
                        <th>Amount</th>
                        <th>Delivery Charges</th>
                        <th>Total Amount</th>
                        <th>Commission</th>
                        <th>Status</th>
                        <th>Order At</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($order_list as $i => $order)
                    <tr>
                        <td><a href="{{ url('admin/allorder/'.$order->id.'/details') }}">{{ $order->id }}</a></td>
                        <td>{{ $order->name }}</td>
                        <td>{{ $order->phone }}</td>
                        <td>{{ $order->location }}</td>
                        <td>{{ $order->address }}</td>
                        <td>{{ Helper::getFormattedPrice($order->sub_total) }}</td>
                        <td>{{ Helper::getFormattedPrice($order->delivery_charges) }}</td>
                        <td>{{ Helper::getFormattedPrice($order->total_amount) }}</td>
                        <td>{{ Helper::getFormattedPrice($order->commission) }}</td>
                        <td>{{ $order->status }}</td>
                        <td>{{ Helper::readableDateFormat($order->created_at) }}</td>
                    </tr>
                    @endforeach

                </tbody>
            </table>

        </div>
        <div class="clearfix"></div>
    </div>

</div>

@endsection
