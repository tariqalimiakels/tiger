@extends("admin.admin_app")

@section("content")
<div id="main">
    <div class="page-header">
    <h2>All Order List Details for {{ $restaurant->restaurant_name }}</h2>
        <a href="{{ URL::to('admin/restaurants/view/'.$restaurant->id.'/orderlist') }}" class="btn btn-default-light btn-xs"><i class="md md-backspace"></i>
            Back</a>

    </div>
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @endif

    <div class="panel panel-default panel-shadow">
        <div class="panel-body">

            <table id="order_data_table" class="table table-striped table-hover dt-responsive" cellspacing="0"
                width="100%">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">S.no</th>
                        <th class="table-web" scope="col">Menu</th>
                        <th class="table-web" scope="col">Item Price</th>
                        <th class="table-web" scope="col">Quantity</th>
                        <th class="table-web" scope="col">Total</th>
                    </tr>
                </thead>

                <tbody>
                    <tr style="background:#90c652;">
                        <td colspan="5" align="center">
                            <h5 style="font-weight:bold; color:white;">{{ $restaurant->restaurant_name }}
                            </h5>
                        </td>
                    </tr>
                    @if ($subTotal = 0) @endif
                    @foreach(\App\Order::getOrderDetails($order_id, $restaurant_id) as
                    $order_item)
                    <tr>
                        <td>
                            {{ $loop->index+1 }}
                        </td>
                        <td>
                            {{ $order_item->item_name }}
                        </td>
                        <td>
                            {{ $order_item->original_price }}
                        </td>
                        <td>
                            {{ $order_item->quantity }}
                        </td>
                        <td>
                            {{ Helper::getFormattedPrice($order_item->item_price) }}
                        </td>
                    </tr>
                    @if ($subTotal = $subTotal + $order_item->item_price) @endif
                    @endforeach
                    <tr class="user-total">
                        <td colspan="4">Sub Total</td>
                        <td>{{  Helper::getFormattedPrice($subTotal) }}</td>
                    </tr>
                    <tr class="user-total">
                        <td colspan="4">Delivery Charges</td>
                        <td>{{  Helper::getFormattedPrice($restaurant->delivery_charge) }}</td>
                    </tr>
                    <tr class="user-total">
                        <td colspan="4">Total ({{ $restaurant->restaurant_name }})</td>
                        <td>{{  Helper::getFormattedPrice($subTotal +  $restaurant->delivery_charge) }}</td>
                    </tr>
                    <tr style="background: #424242;">
                        <td colspan="5" align="center">
                            <h5 style="font-weight:bold; color:white;">Grand Total
                            </h5>
                        </td>
                    </tr>
                    <tr class="user-total">
                        <td colspan="4">Sub Total</td>
                        <td>{{  Helper::getFormattedPrice($order->sub_total) }}</td>
                    </tr>
                    <tr class="user-total">
                        <td colspan="4">Delivery Charges</td>
                        <td>{{  Helper::getFormattedPrice($order->delivery_charges) }}</td>
                    </tr>
                    <tr class="user-total">
                        <td colspan="4">Total ({{ $restaurant->restaurant_name }})</td>
                        <td>{{  Helper::getFormattedPrice($order->total_amount) }}</td>
                    </tr>
                    
                </tbody>
            </table>

        </div>
        <div class="clearfix"></div>
    </div>

</div>

@endsection
