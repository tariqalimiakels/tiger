@extends("admin.admin_app")

@section("content")
<div id="main">
    <div class="page-header">


        <h2>Order List</h2>
        <a href="{{ URL::to('admin/restaurants/view/'.$restaurant_id) }}" class="btn btn-default-light btn-xs"><i
                class="md md-backspace"></i> Back Restaurant</a>
    </div>
    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        {{ Session::get('flash_message') }}
    </div>
    @endif

    <div class="panel panel-default panel-shadow">
        <div class="panel-body">

            <table id="order_data_table" class="table table-striped table-hover dt-responsive" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        <th>Order No</th>
                        <th>User</th>
                        <th>Mobile</th>
                        <th>Area</th>
                        <th>Address</th>
                        <th>Amount</th>
                        <th>Delivery Charges</th>
                        <th>Total Amount</th>
                        <th>Status</th>
                        <th>Order At</th>
                        <th class="text-center width-100">Action</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($order_list as $i => $order)
                    <tr>
                        <td><a href="{{ url('admin/restaurants/view/'.$restaurant_id.'/orderlist/'.$order->id.'/details') }}">{{ $order->id }}</a></td>
                        <td>{{ $order->name }}</td>
                        <td>{{ $order->phone }}</td>
                        <td>{{ $order->location }}</td>
                        <td>{{ $order->address }}</td>
                        <td>{{ Helper::getFormattedPrice($order->sub_total) }}</td>
                        <td>{{ Helper::getFormattedPrice($order->delivery_charges) }}</td>
                        <td>{{ Helper::getFormattedPrice($order->total_amount) }}</td>
                        <td>{{ $order->status }}</td>
                        <td>{{ Helper::readableDateFormat($order->created_at) }}</td>
                        <td class="text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default-dark dropdown-toggle"
                                    data-toggle="dropdown" aria-expanded="false">
                                    Actions <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li><a
                                            href="{{ url('admin/restaurants/view/'.$restaurant_id.'/orderlist/'.$order->id.'/Pending') }}"><i
                                                class="md md-lock"></i> Pending</a></li>
                                    <li><a
                                            href="{{ url('admin/restaurants/view/'.$restaurant_id.'/orderlist/'.$order->id.'/Processing') }}"><i
                                                class="md md-loop"></i> Processing</a></li>
                                    <li><a
                                            href="{{ url('admin/restaurants/view/'.$restaurant_id.'/orderlist/'.$order->id.'/Completed') }}"><i
                                                class="md md-done"></i> Completed</a></li>
                                    {{-- <li><a href="{{ url('admin/restaurants/view/'.$restaurant_id.'/orderlist/'.$order->id.'/Cancel') }}"><i
                                        class="md md-cancel"></i> Cancel</a></li>
                                    <li><a
                                            href="{{ url('admin/restaurants/view/'.$restaurant_id.'/orderlist/'.$order->id) }}"><i
                                                class="md md-delete"></i> Delete</a></li> --}}
                                </ul>
                            </div>

                        </td>


                    </tr>
                    @endforeach

                </tbody>
            </table>

            {{ $order_list->links() }}

        </div>
        <div class="clearfix"></div>
    </div>

    

</div>

@endsection
