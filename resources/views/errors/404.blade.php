@extends("layouts.app")

@section('head_title', config('widgets.about_title') .' | '.config('settings.site_name') )
@section('head_url', Request::url())

@section("content")

@include("_particles.section-cover-static", ['title' => '', 'description' =>
'', 'cover_photo' => 'client_assets/images/default-cover.jpg'])

<div class="what-we-do">
    <div class="container about_block">

        <div class="col-md-12" style="text-align: center;">

            <h1 style="font-size: 128px;font-weight: 800; text-align:center">404!</h1>
            <a style="font-size: 20px;" href="{{ url('/') }}">Go to home page</a>
        </div>

    </div>
</div>


@endsection
