$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".menu-item").click(function() {
        const ele = $(this).closest(".strip");
        $('#modalTitle').text(ele.data('name'));
        $('#modalName').text(ele.data('name'));
        $('.modalPrice').html(ele.data('price'));
        $('#modalID').text(ele.data('id'));
        $("#modalImg").attr("src", ele.data('image'));
        $('#modalDescription').html(ele.data('description'));
        $('#addToCart1 .add-to-cart').attr('href', ele.data('url'));
        $('#productModal').modal('show');
    });

    $(".restaurant-info").click(function() {
        $('#restaurantModal').modal('show');
    })

    $('.select2').select2({
        width: '100%',
    });

    $('.select2-menu').select2({
        dropdownParent: $('#productModal')
    });

    $('.select2').addClass('form-control');
    $('.select2-selection').css('border', '0');
    $('.select2-selection__arrow').css('top', '10px');
    $('.select2-selection__rendered').css('color', '#8898aa');
    $('.select2-selection__rendered').css('line-height', '22px');

    //If item is added into cart is added
    if ($('input[name=is_item_added]').length > 0) {
        openNav();
    }

    $(".find-your-meal").click(function() {
        alert('');
    });

    $("#searchrestaurant, #searchrestaurantnav").submit(function(event) {

        if ($(this).find(".select2").val() !== "") {
            return;
        }

        alert("Select your delivery location first!")
        event.preventDefault();
    });

    $(".check-now").click(function(e) {
        e.preventDefault();
        $(".restaurant-not-available").addClass("d-none")

        var location_id = $('.select2').val();
        var restaurant_id = $('#restaurantID').val();

        $.ajax({
            type: 'POST',
            url: api_url + '/restaurant/check-delivery',
            data: {
                location_id,
                restaurant_id
            },
            dataType: 'json',
            success: function(response) {
                if (response.success) {

                    //if deliver
                    if (response.data.is_deliver) {
                        //alert('This restaurant can be delivered at your location.');
                        location.reload();
                    } else {
                        alert('This restaurant can not be delivered at your location. Please choose any other restaurant.');
                    }
                }

            },
            error: function(response) {
                alert("someting went wrong. Please try again later.");
            }
        })
    });

    $('.foodTypeSlider ul').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        prevArrow: "<button type='button' class='slick-prev'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
        nextArrow: "<button type='button' class='slick-next'><i class='fa fa-angle-right' aria-hidden='true'></i></button>",
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    })

    $('.deliveryAddress, .check-location').on('click', function() {
        $('#productModal').modal('hide');
        $('.deliveryAddress').toggleClass('active');
    });
    $('.btn-accordion').on('click', function() {
        $(this).parents('.menuType').toggleClass('activeAccordion')
    })

})