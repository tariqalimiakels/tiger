function openNav() {
    document.getElementById("cartSideNav").classList.add('active')
}

function closeNav() {
    document.getElementById("cartSideNav").classList.remove('active')
}

$(window).scroll(function() {
    if ($(window).scrollTop() >= 1) {
        $('#navbar-main').addClass('custom-nav');
    } else {
        $('#navbar-main').removeClass('custom-nav');
    }
});