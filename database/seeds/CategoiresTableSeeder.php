<?php

use Illuminate\Database\Seeder;

class CategoiresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Categories', 10)->create();
    }
}
