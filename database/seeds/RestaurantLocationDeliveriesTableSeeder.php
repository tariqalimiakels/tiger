<?php

use Illuminate\Database\Seeder;

class RestaurantLocationDeliveriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\RestaurantLocationDelivery', 10)->create();
    }
}
