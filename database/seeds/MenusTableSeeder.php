<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $restaurants = App\Restaurants::all();
        foreach($restaurants as $restaurant){
            factory('App\Menu', 20)->create(['restaurant_id' => $restaurant->id]);
        }
    }
}
