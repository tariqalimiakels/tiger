<?php

use Illuminate\Database\Seeder;

class CommissionSlabsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $slabs = [
            ['min_value' => 1, 'max_value' => 100, 'commission' => 10],
            ['min_value' => 101, 'max_value' => 200, 'commission' => 20],
            ['min_value' => 201, 'max_value' => 300, 'commission' => 30],
            ['min_value' => 301, 'max_value' => 400, 'commission' => 40],
            ['min_value' => 401, 'max_value' => 500, 'commission' => 50],
            ['min_value' => 501, 'max_value' => 600, 'commission' => 60],
            ['min_value' => 601, 'max_value' => 700, 'commission' => 70],
            ['min_value' => 701, 'max_value' => 800, 'commission' => 80],
            ['min_value' => 801, 'max_value' => 900, 'commission' => 90],
            ['min_value' => 901, 'max_value' => 1000, 'commission' => 100],
            ['min_value' => 1001, 'max_value' => 10000, 'commission' => 200],
        ];

        foreach($slabs as $slab){

            DB::table('commission_slabs')->insert([
                'min_value' => $slab['min_value'],
                'max_value' => $slab['max_value'],
                'commission' => $slab['commission'],
            ]);
        }
    }
}
