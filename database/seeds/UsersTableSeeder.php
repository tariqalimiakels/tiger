<?php

use App\Events\Inst;
use App\Menu;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        // Create admin account
        DB::table('users')->insert([
            'usertype' => 'Admin',
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
            'image_icon' => 'admin-965bf2e0f3108983112bb705d2db4304',
            'remember_token' => Str::random(10),
            'status' => User::STATUS_ACTIVE,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('users')->insert([
            'usertype' => 'User',
            'name' => 'User',
            'email' => 'tariq.ali@mikaels.com',
            'password' => bcrypt('admin'),
            'image_icon' => 'admin-965bf2e0f3108983112bb705d2db4304',
            'remember_token' => Str::random(10),
            'status' => User::STATUS_ACTIVE,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        
        DB::table('widgets')->insert([
            'footer_widget1_title' => 'About Restaurant',
            'footer_widget1_desc' => 'Are you hungry? and cannot go out due to lockdown? Then foodin is the right place for you! foodin, offers you a long and detailed list of the best restaurants near you. Whether it is a delicious Pizza, Burger, Biryani or any kind of Fast Food you are craving, foodin has an extensive roaster of 50 restaurants. There are plenty of restaurants available for you to order food online with home delivery. foodin is only available in Karachi for now.',
            'footer_widget2_title' => 'List your restaurant on foodin?',
            'footer_widget2_desc' => "Would you like thousands of new customers to taste your amazing food? So would we! It's simple: you can list your menu online, and deliver them to hungry people. Interested? Let's start our partnership today! </br> <a href='/register'>Get Started</a>",
            'footer_widget3_title' => 'Contact Info',
            'footer_widget3_address' => 'Clifton, Karachi - Pakistan',
            'footer_widget3_phone' => '+92 332 2539825',
            'footer_widget3_email' => 'admin@example.com',
            'about_title' => 'About Us',
            'about_desc' => 'Are you hungry? and cannot go out due to lockdown? Then foodin is the right place for you! foodin, offers you a long and detailed list of the best restaurants near you. Whether it is a delicious Pizza, Burger, Biryani or any kind of Fast Food you are craving, foodin has an extensive roaster of 50 restaurants. There are plenty of restaurants available for you to order food online with home delivery. foodin is only available in Karachi for now.',
            'need_help_title' => 'Need Help?',
            'need_help_phone' => '+92 332 2539825',
            'need_help_time' => 'Monday to Sunday 9 AM - 9 PM'
             
        ]);
        
        DB::table('settings')->insert([            
            'site_name' => 'Foodia',
            'site_title' => 'Order Food Online from your favorite restaurants',
            'currency_symbol' => 'Rs.',
            'site_email' => 'contact@foodia.pk',
            'site_logo' => 'logo.png',
            'site_favicon' => 'favicon.png',
            'site_description' => 'Get food delivered from your favorite restaurants - Best local restaurants near you - Fast and easy delivery - Large variety of cuisines and dishes',
            'site_copyright' => '© 2020',
            'home_slide_image1' => 'home_slide_image1.png',
            'home_slide_image2' => 'home_slide_image2.png',
            'home_slide_image3' => 'home_slide_image3.png',
            'page_bg_image' => 'page_bg_image.png',
            'total_restaurant' => '43',
            'total_people_served' => '112',
            'total_registered_users' => '77',
            'commission_type' => 'percentage'
        ]);
         
		DB::table('restaurant_types')->insert([
			'type' => 'Fast Food',
			'type_slug' => 'fast-food',
            'type_image' => 'American_1458535213'
		]);
		
		DB::table('restaurant_types')->insert([
            'type' => 'Bar B Q',
            'type_slug' => 'bar-b-q',
            'type_image' => 'Chinese_1458535609'
        ]);

        DB::table('restaurant_types')->insert([
            'type' => 'Biryani Stuff',
            'type_slug' => 'biryani-stuff',
            'type_image' => 'Indian_1458535662'   
        ]);

        DB::table('restaurant_types')->insert([
            'type' => 'Ghar Ka Khana',
            'type_slug' => 'ghar-ka-khana',
            'type_image' => 'Mexican_1458535796'
        ]);

        DB::table('restaurant_types')->insert([
            'type' => 'Pizza',
            'type_slug' => 'pizza',
            'type_image' => 'Sushi_1458535621'
        ]);

        DB::table('restaurant_types')->insert([
            'type' => 'Sweet Cravings',
            'type_slug' => 'sweet-cravings',
            'type_image' => 'Thai_1458535292'
        ]); 

        DB::table('restaurant_types')->insert([
            'type' => 'Mixed',
            'type_slug' => 'mixed',
            'type_image' => 'Mexican_1458535796'
        ]); 

        $locations = App\Location::all();
        factory('App\User', 10)->create()->each(function($u) use ($locations) {
            $u->restaurant()->save(factory(App\Restaurants::class)->make())->each(function($r) use ($locations) {
                $r->categories()->saveMany(factory(App\Categories::class, 2)->make());
                //$r->menus()->save(factory(App\Menu::class)->make(['menu_cat' => $r->id]));
                $r->locations()->attach($locations->random(rand(1, 3))->pluck('id')->toArray(), ['delivery_time'=> '1 hour']);
            });
        });
		
       // factory('App\User', 20)->create();
    }
}
