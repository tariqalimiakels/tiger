<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
	protected $toTruncate = ['users'];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
 
        $this->call(LocationsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        //$this->call(RestaurantLocationDeliveriesTableSeeder::class);
        //$this->call(CategoiresTableSeeder::class);
        $this->call(MenusTableSeeder::class);
        $this->call(CommissionSlabsSeeder::class);

        Model::reguard();
    }
}
