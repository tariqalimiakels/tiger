<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\RestaurantLocationDelivery;
use Faker\Generator as Faker;

$factory->define(RestaurantLocationDelivery::class, function (Faker $faker) {
    return [
        // 'restaurant_id' => function () {
        //     return App\Restaurants::inRandomOrder()->first()->id;
        // },
        'location_id' => function () {
            return App\Location::inRandomOrder()->first()->id;
        },
        'delivery_time' => '1 hour'
    ];
});
