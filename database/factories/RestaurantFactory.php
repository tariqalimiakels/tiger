<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Restaurants;
use Faker\Generator as Faker;

$factory->define(Restaurants::class, function (Faker $faker) {
    return [
        'restaurant_type' => function () {
            return App\Types::inRandomOrder()->first()->id;
        },
        'restaurant_name' => $faker->name,
        'restaurant_slug' => $faker->slug,
        'restaurant_description' => $faker->realText(200),
        'restaurant_address' => $faker->address,
        'delivery_charge' => $faker->numberBetween(10, 150),
        'delivery_time' => '30 minutes',
        'min_order' =>  $faker->numberBetween(100, 500),
        'max_order' =>  $faker->numberBetween(1000, 3000),
        'restaurant_logo' => 'zabs_1587125331',
        'commission_percentage' => $faker->numberBetween(5, 10),
        'open_monday' => '09:00',
        'close_monday' => '15:00',
        'open_tuesday' => '10:00',
        'close_tuesday' => '16:00',
        'open_wednesday' => '10:00',
        'close_wednesday' => '17:00',
        'open_thursday' => '11:00',
        'close_thursday' => '18:00',
        'open_friday' => '12:00',
        'close_friday' => '19:00',
        'open_saturday' => '13:00',
        'close_saturday' => '20:00',
        'open_sunday' => '14:00',
        'close_sunday' => '23:00',
        'review_avg' => '0',
    ];
});
