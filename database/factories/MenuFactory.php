<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Menu;
use Faker\Generator as Faker;

$factory->define(Menu::class, function (Faker $faker, $params) {
    $restauranId = $params['restaurant_id'];
    return [
        // 'restaurant_id' => function () {
        //     return App\Restaurants::inRandomOrder()->first()->id;
        // },
        'menu_cat' => function () use ($restauranId) {
            return App\Categories::inRandomOrder()->where('restaurant_id', $restauranId)->first()->id;
        },
        'menu_name' => $faker->name,
        'description' => $faker->realText(200),
        'price' => $faker->numberBetween(100, 2000),
        'menu_image' => 'Amara Brakus_1590924266',
        'status' => '1',
    ];
});
