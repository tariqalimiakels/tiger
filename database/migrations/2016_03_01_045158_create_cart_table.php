<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('cart', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');     
            $table->integer('restaurant_id'); 
            $table->integer('item_id');            
            $table->string('item_name');
            $table->decimal('original_price');     
            $table->decimal('item_price');     
            $table->integer('quantity')->length(2); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cart');
    }
}
