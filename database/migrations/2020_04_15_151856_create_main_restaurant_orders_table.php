<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainRestaurantOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_restaurant_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('main_order_id');
            $table->integer('restaurant_id');
            $table->decimal('sub_total');
            $table->decimal('delivery_charges');
            $table->decimal('total_amount');
            $table->decimal('commission');
            $table->decimal('paid_amount');
            $table->decimal('paid_commission');
            $table->string('paid_to_admin')->default('no');
            $table->string('status');
            $table->longText('comment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('main_restaurant_orders');
    }
}
