<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('restaurant_id');                  
            $table->string('category_name');            
            $table->string('open_monday')->nullable();
            $table->string('close_monday')->nullable();
            $table->string('open_tuesday')->nullable();
            $table->string('close_tuesday')->nullable();
            $table->string('open_wednesday')->nullable();
            $table->string('close_wednesday')->nullable();
            $table->string('open_thursday')->nullable();
            $table->string('close_thursday')->nullable();
            $table->string('open_friday')->nullable();
            $table->string('close_friday')->nullable();
            $table->string('open_saturday')->nullable();
            $table->string('close_saturday')->nullable();
            $table->string('open_sunday')->nullable();           
            $table->string('close_sunday')->nullable();      
            $table->integer('status')->length(1)->default(1); 
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
