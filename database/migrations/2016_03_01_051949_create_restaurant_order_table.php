<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('main_order_id');
            $table->integer('restaurant_id');
            $table->integer('item_id');             
            $table->string('item_name');             
            $table->decimal('original_price');
            $table->integer('quantity');
            $table->decimal('item_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('restaurant_order');
    }
}
