<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommissionSlabs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commission_slabs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->default('flat');
            $table->decimal('min_value');
            $table->decimal('max_value');
            $table->decimal('commission');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commission_slabs');
    }
}
