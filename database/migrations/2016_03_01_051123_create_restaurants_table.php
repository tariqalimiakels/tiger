<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('restaurant_type');                    
            $table->string('restaurant_name');
            $table->string('restaurant_slug');
            $table->longtext('restaurant_description');
            $table->text('restaurant_address');
            $table->decimal('delivery_charge')->default(0);
            $table->decimal('min_order')->default(0);
            $table->decimal('max_order')->default(0);
            $table->string('delivery_time'); 
            $table->string('restaurant_logo');           
            $table->string('restaurant_bg');
            $table->integer('commission_percentage');
            $table->string('open_monday')->nullable();
            $table->string('close_monday')->nullable();
            $table->string('open_tuesday')->nullable();
            $table->string('close_tuesday')->nullable();
            $table->string('open_wednesday')->nullable();
            $table->string('close_wednesday')->nullable();
            $table->string('open_thursday')->nullable();
            $table->string('close_thursday')->nullable();
            $table->string('open_friday')->nullable();
            $table->string('close_friday')->nullable();
            $table->string('open_saturday')->nullable();
            $table->string('close_saturday')->nullable();
            $table->string('open_sunday')->nullable();           
            $table->string('close_sunday')->nullable();           
            $table->integer('review_avg');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('restaurants');
    }
}
