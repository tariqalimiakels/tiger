<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Location extends Model
{
    protected $fillable = ['city_id', 'location_name'];

    public $timestamps = false;

    public static function getDeliveryLocationsBelongsToRestaurants(){
        $query = DB::table('locations')
                    ->join('restaurant_location_deliveries', 'restaurant_location_deliveries.location_id', '=', 'locations.id')
                    ->groupBy("locations.id")
                    ->orderBy('location', 'asc')
                    ->get();

        return $query;
    }
    
}
