<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Order;

class MainOrder extends Model
{
    protected $table = 'main_orders';

    protected $fillable = ['user_id','total_amount','comment', 'created_at','updated_at'];

    public static function getOwnerOrders($restaurant_Id){

        $query = DB::table("main_orders")
            ->select('main_restaurant_orders.*', 'main_orders.phone', 'main_orders.address', 'main_orders.created_at', 'users.name', 'locations.location')
            ->join('main_restaurant_orders', 'main_restaurant_orders.main_order_id', '=', 'main_orders.id')
            ->join('users', 'users.id', '=', 'main_orders.user_id')
            ->join('locations', 'locations.id', '=', 'main_orders.location_id')
            ->where('main_restaurant_orders.restaurant_id', $restaurant_Id)
            ->orderBy('main_orders.id', 'desc')
            ->paginate(20);

        return $query;
    }

    public static function getAllOrders(){

        $query = DB::table("main_orders")
            ->select('main_orders.*', 'users.name', 'locations.location', DB::raw("(SELECT SUM(commission) FROM main_restaurant_orders WHERE main_order_id = main_orders.id) as commission"))
            ->join('users', 'users.id', '=', 'main_orders.user_id')
            ->join('locations', 'locations.id', '=', 'main_orders.location_id')
            ->orderBy('main_orders.id', 'desc')
            ->get();

        return $query;
    }
	 
}
