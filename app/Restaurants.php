<?php

namespace App;

use App\Categories;
use App\Menu;
use App\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Restaurants extends Model
{
    protected $table = 'restaurants';

    protected $fillable = ['type', 'restaurant_name','restaurant_slug','restaurant_description','restaurant_address','delivery_charge','restaurant_logo'];


    public $timestamps = false;
 
    public function restaurants()
    {
        return $this->hasMany('App\Restaurants', 'id');
    }

    public function categories()
    {
        return $this->hasMany('App\Categories', 'restaurant_id');
    }

    public function locations()
    {
        return $this->belongsToMany('App\Location', 'restaurant_location_deliveries', 'restaurant_id')->withPivot('delivery_time');;
    }

    public function menus()
    {
        return $this->hasMany('App\Menu', 'restaurant_id');
    }
    
    public static function getRestaurantsInfo($id)
    {
        return Restaurants::find($id);
    }

    public static function getUserRestaurant($id)
    {
        return Restaurants::where('user_id', $id)->count();
    }


    public static function getMenuCategories($id)
    {
        return Categories::where('restaurant_id', $id)->count();
    }

    public static function getMenuItems($id)
    {
        return Menu::where('restaurant_id', $id)->count();
    }

    public static function getOrders($id)
    {
        return Order::where('restaurant_id', $id)->count();
    }

    public static function getTotalRestaurants()
    {
        return Restaurants::count();
    }


    public static function searchByKeyword($keyword, $currentTime, $openColumn, $closeColumn, $isTimeRange = true, $isNoOrderRestaurants = true, $status = "")
    {
        $restaurants = DB::table('restaurants')
                ->join('menu', 'menu.restaurant_id', '=', 'restaurants.id')
                ->join('users', 'users.id', '=', 'restaurants.user_id')
                // ->join('restaurant_location_deliveries', function ($join) use ($location_id) {
                //     $join->on('restaurant_location_deliveries.restaurant_id', '=', 'restaurants.id')->where('restaurant_location_deliveries.location_id', '=', $location_id);
                // })
                // ->join('categories', function ($join) use ($keyword) {
                //     $join->on('categories.restaurant_id', '=', 'restaurants.id')->where('categories.category_name', "LIKE", "%$keyword%");
                // })
                ->leftJoin('main_restaurant_orders', function ($join) use ($isNoOrderRestaurants) {
                    $join = $join->on('main_restaurant_orders.restaurant_id', '=', 'restaurants.id');
                    if ($isNoOrderRestaurants) {
                        $mainOrder = MainOrder::whereDate('created_at', DB::raw('CURDATE()'))->get()->pluck('id')->toArray();
                        $join->whereIn('main_restaurant_orders.main_order_id', $mainOrder);
                    }
                });
        
        if ($isNoOrderRestaurants && $status == "") {
            $restaurants = $restaurants->where('main_restaurant_orders.id', null);
        }
        
        if ($status != "") {
            $restaurants = $restaurants->where('main_restaurant_orders.status', $status);
        }
        
        if ($isTimeRange) {
            $restaurants = $restaurants->whereRaw("('$currentTime' between restaurants.`$openColumn` and restaurants.`$closeColumn`)");
        }

        if (session("location")) {
            $location = session("location");
            $restaurants = $restaurants->join('restaurant_location_deliveries', function ($join) use ($location) {
                $join->on('restaurant_location_deliveries.restaurant_id', '=', 'restaurants.id')->where('restaurant_location_deliveries.location_id', '=', $location);
            });
        }

        $restaurants = $restaurants->select('restaurants.*')
                           ->where('users.status', User::STATUS_ACTIVE)
                           ->groupBy('restaurants.id')
                           ->orderBy('restaurants.restaurant_name', 'asc')
                           ->paginate(10);
                           //->toSql();
        return $restaurants;
    }

    public static function getByType($type, $currentTime, $openColumn, $closeColumn, $isTimeRange = true, $isNoOrderRestaurants = true, $status = "")
    {
        $restaurants = DB::table('restaurants')
                        ->join('restaurant_types', 'restaurants.restaurant_type', '=', 'restaurant_types.id')
                        ->join('menu', 'menu.restaurant_id', '=', 'restaurants.id')
                        ->join('users', 'users.id', '=', 'restaurants.user_id')
                        ->leftJoin('main_restaurant_orders', function ($join) use ($isNoOrderRestaurants) {
                            $join = $join->on('main_restaurant_orders.restaurant_id', '=', 'restaurants.id');
                            if ($isNoOrderRestaurants) {
                                $mainOrder = MainOrder::whereDate('created_at', DB::raw('CURDATE()'))->get()->pluck('id')->toArray();
                                $join->whereIn('main_restaurant_orders.main_order_id', $mainOrder);
                            }
                        });

        if ($isNoOrderRestaurants && $status == "") {
            $restaurants = $restaurants->where('main_restaurant_orders.id', null);
        }

        if ($status != "") {
            $restaurants = $restaurants->where('main_restaurant_orders.status', $status);
        }

        if ($isTimeRange) {
            $restaurants = $restaurants->whereRaw("('$currentTime' between restaurants.`$openColumn` and restaurants.`$closeColumn`)");
        }

        if (session("location")) {
            $location = session("location");
            $restaurants = $restaurants->join('restaurant_location_deliveries', function ($join) use ($location) {
                $join->on('restaurant_location_deliveries.restaurant_id', '=', 'restaurants.id')->where('restaurant_location_deliveries.location_id', '=', $location);
            });
        }
                           
        $restaurants = $restaurants->select('restaurants.*', 'restaurant_types.type')
                           ->where('restaurant_types.type_slug', '=', $type)
                           ->where('users.status', User::STATUS_ACTIVE)
                           ->groupBy('restaurants.id')
                           ->orderBy('restaurants.restaurant_name', 'asc')
                           ->paginate(10);
        
        return $restaurants;
    }

    public static function getPopularRestaurants($currentTime, $openColumn, $closeColumn, $isTimeRange = true, $isNoOrderRestaurants = true, $status = "")
    {
        $restaurants = DB::table('restaurants')
            ->join('menu', 'menu.restaurant_id', '=', 'restaurants.id')
            ->join('users', 'users.id', '=', 'restaurants.user_id')
            ->leftJoin('main_restaurant_orders', function ($join) use ($isNoOrderRestaurants) {
                $join = $join->on('main_restaurant_orders.restaurant_id', '=', 'restaurants.id');
                if ($isNoOrderRestaurants) {
                    $mainOrder = MainOrder::whereDate('created_at', DB::raw('CURDATE()'))->get()->pluck('id')->toArray();
                    $join->whereIn('main_restaurant_orders.main_order_id', $mainOrder);
                }
            });
        
        if ($isNoOrderRestaurants && $status == "") {
            $restaurants = $restaurants->where('main_restaurant_orders.id', null);
        }

        if ($status != "") {
            $restaurants = $restaurants->where('main_restaurant_orders.status', $status);
        }

        if ($isTimeRange) {
            $restaurants = $restaurants->whereRaw("('$currentTime' between restaurants.`$openColumn` and restaurants.`$closeColumn`)");
        }

        if (session("location")) {
            $location = session("location");
            $restaurants = $restaurants->join('restaurant_location_deliveries', function ($join) use ($location) {
                $join->on('restaurant_location_deliveries.restaurant_id', '=', 'restaurants.id')->where('restaurant_location_deliveries.location_id', '=', $location);
            });
        }
                            
        $restaurants = $restaurants->select('restaurants.*')
                            ->where('users.status', User::STATUS_ACTIVE)
                            ->groupBy('restaurants.id')
                            ->orderBy('restaurants.restaurant_name', 'asc')
                            ->paginate(20);
                            //->toSql();
        
        return $restaurants;
    }
}
