<?php

namespace App;

use App\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Categories extends Model
{
    protected $table = 'categories';

    protected $fillable = ['category_name'];


	public $timestamps = false;
 
	public function news()
    {
        return $this->hasMany('App\News', 'cat_id');
    }
	
	public static function getCategoryInfo($id) 
    { 
		return Categories::find($id);
    }
    
    public static function getCategoryByMenu($menuId)
    {
        $category = DB::table('categories')
                    ->select('categories.*', 'menu.*')
                    ->join('menu', 'menu.menu_cat', '=', 'categories.id')
                    ->where('menu.id', $menuId)->first();

        return $category;
    }

    public static function checkIfCategoryAvailbleNow($cateogryId, $openColumn, $closeColumn)
    {
        $currentTime = Helper::getBufferTime();
        $category = DB::table('categories')
                    ->where('id', $cateogryId)
                    ->whereRaw("('$currentTime' between categories.`$openColumn` and categories.`$closeColumn`)")
                    ->first();

        return $category;
    }
}
