<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MainRestaurantOrder extends Model
{
    protected $table = 'main_restaurant_orders';
    protected $fillable = ['main_order_id','restaurant_id','sub_total','delivery_charges','total_amount','commission','status','comment'];
    public $timestamps = false;

    public static function restaurantByMainOrderId($mainOrderId)
    {
        $query = DB::table('main_restaurant_orders')
                ->select('main_restaurant_orders.*', 'restaurants.restaurant_name', 'main_orders.location_id', 'restaurants.delivery_time')
                ->join('restaurants', 'restaurants.id', '=', 'main_restaurant_orders.restaurant_id')
                ->join('main_orders', 'main_orders.id', '=', 'main_restaurant_orders.main_order_id')
                ->where('main_restaurant_orders.main_order_id', $mainOrderId)->get();

        //Delivery charges by location
        foreach ($query as $ct) {
            $ct->delivery_time = static::getDeliverTime($ct->restaurant_id, $ct->location_id, $ct->delivery_time);
        }

        return $query;
    }

    public static function getDeliverTime($restaurantId, $locationId, $defaultDeliveryTime)
    {
        $deliveryTime = $defaultDeliveryTime;
        $delivery = DB::table('restaurant_location_deliveries')->where('restaurant_id', $restaurantId)->where('location_id', $locationId)->first();
        if ($delivery != "") {
            if ($delivery->delivery_time != "") {
                $deliveryTime = $delivery->delivery_time;
            }
        }

        return $deliveryTime;
    }
}
