<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    protected $table = 'restaurant_order';

    protected $fillable = ['item_name', 'item_price','quantity','created_date'];

    public $timestamps = false;
    
    public static function geUniqueRestaurants($mainOrderId)
    {
        $query = DB::table('restaurant_order')
            ->select('restaurant_order.restaurant_id', 'restaurants.restaurant_name', 'restaurants.delivery_charge')
            ->join('restaurants', 'restaurants.id', '=', 'restaurant_order.restaurant_id')
            ->where('restaurant_order.main_order_id', $mainOrderId)
            ->groupBy('restaurant_order.restaurant_id')
            ->get();

        return $query;
    }
 

    public static function getOrderDetails($mainOrderId, $restaurantId)
    {
        $query = DB::table('restaurant_order')
            ->select('restaurant_order.*')
            ->where('restaurant_order.main_order_id', $mainOrderId)
            ->where('restaurant_order.restaurant_id', $restaurantId)
            ->get();

        return $query;
    }

    public static function ownerOrderlistDetails($restaurantId){
        $query = DB::table('main_restaurant_orders')
            ->select('main_restaurant_orders.*', 'main_orders.phone', 'main_orders.address', 'users.name', 'locations.location')
            ->join('main_orders', 'main_orders.id', '=', 'main_restaurant_orders.main_order_id')
            ->join('users', 'users.id', '=', 'main_orders.user_id')
            ->join('locations', 'locations.id', '=', 'main_orders.location_id')
            ->where('main_restaurant_orders.restaurant_id', $restaurantId)
            ->first();

        return $query;
    }
}
