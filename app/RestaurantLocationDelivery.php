<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RestaurantLocationDelivery extends Model
{
    protected $fillable = ['restaurant_id', 'location_id'];

    public $timestamps = false;
    
}
