<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cart extends Model
{
    protected $table = 'cart';

    protected $fillable = ['item_name', 'item_price','quantity'];


    public $timestamps = false;
 
    public static function getCart($userId)
    {
        $cart = DB::table('cart')
            ->select('cart.*', 'restaurants.restaurant_name', 'menu.menu_image')
            ->join('restaurants', 'restaurants.id', '=', 'cart.restaurant_id')
            ->join('menu', 'menu.id', '=', 'cart.item_id')
            ->where('cart.user_id', $userId)->orderBy('cart.id')->get();

        return $cart;
    }

    public static function getDeliveryCharges($userId)
    {
        $restaurants = static::getCartUniqueRestaurants($userId);

        $deliveryCharges = 0;
        foreach ($restaurants as $restaurant) {
            $deliveryCharges = $deliveryCharges + $restaurant->delivery_charges_custom;
        }

        return $deliveryCharges;
    }

    public static function getCartUniqueRestaurants($userId)
    {
        $cart = DB::table('cart')
            ->select('restaurants.*', 'cart.*', 'users.email')
            ->join('restaurants', 'restaurants.id', '=', 'cart.restaurant_id')
            ->join('users', 'users.id', '=', 'restaurants.user_id')
            ->where('cart.user_id', $userId)
            ->groupBy('cart.restaurant_id')
            ->get();

        //Delivery charges by location
        foreach ($cart as $ct) {
            $ct->delivery_charges_custom = static::calculateDeliverCharges($ct->restaurant_id, session('location'), $ct->delivery_charge);
        }

        return $cart;
    }

    public static function calculateDeliverCharges($restaurantId, $locationId, $defaultDeliveryCharges)
    {
        $deliveryCharges = $defaultDeliveryCharges;
        $delivery = DB::table('restaurant_location_deliveries')->where('restaurant_id', $restaurantId)->where('location_id', $locationId)->first();
        if ($delivery != "") {
            if ($delivery->delivery_charges != "") {
                $deliveryCharges = $delivery->delivery_charges;
            }
        }

        return $deliveryCharges;
    }
}
