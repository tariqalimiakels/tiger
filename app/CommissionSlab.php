<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CommissionSlab extends Model
{
    protected $fillable = ['min_value', 'max_value', 'commission'];
    public $timestamps = false;
    
}
