<?php

namespace App\Http\Controllers;

use App\Categories;
use App\RestaurantLocationDelivery;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function weekly()
    {
        return [
                'Monday' => ['open_monday', 'close_monday'],
                'Tuesday' => ['open_tuesday', 'close_tuesday'],
                'Wednesday' => ['open_wednesday', 'close_wednesday'],
                'Thursday' => ['open_thursday', 'close_thursday'],
                'Friday' => ['open_friday', 'close_friday'],
                'Saturday' => ['open_saturday', 'close_saturday'],
                'Sunday' => ['open_sunday', 'close_sunday'],
            ];
    }

    public function todayNameColumns()
    {
        $now = Carbon::now();
        $todayName = $now->dayName; //Monday, Tuesday
        $todayNameColumns = $this->weekly()[$todayName];

        return [
                'openColumn' => $todayNameColumns[0],
                'closeColumn' => $todayNameColumns[1],
            ];
    }

    public function getBufferTime()
    {
        $now = Carbon::now();
        $currentTime = $now->format('H:i');

        return $currentTime;
    }

    public function assignCategoriesNames($restaurants)
    {
        foreach ($restaurants as $restaurant) {
            $categories = Categories::select(DB::raw("group_concat(category_name SEPARATOR ', ') as category_names"))->where("restaurant_id", $restaurant->id)->first();

            $restaurant->category_names = $categories->category_names;
        }

        return $restaurants;
    }

    public function assignDeliveryChargesFrom($restaurants)
    {
        foreach ($restaurants as $restaurant) {
            $deliveryStartsFrom = RestaurantLocationDelivery::where("restaurant_id", $restaurant->id)->min('delivery_charges');
            $restaurant->delivery_starts_from = $deliveryStartsFrom;
        }

        return $restaurants;
    }
}
