<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\User;
use App\Order;
use App\Categories;
use App\CommissionSlab;
use App\Restaurants;

use App\Http\Requests;
use App\Location;
use App\MainOrder;
use App\MainRestaurantOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Intervention\Image\Facades\Image;

class OrderController extends MainAdminController
{
    public function __construct()
    {
        $this->middleware('auth');
          
        parent::__construct();
    }
    public function orderlist($id)
    {
        $order_list = MainOrder::getOwnerOrders($id);
        
        if (Auth::User()->usertype!="Admin") {
            \Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');
        }
        
        $restaurant_id=$id;

        return view('admin.pages.order_list', compact('order_list', 'restaurant_id'));
    }

    public function orderlist_details($id, $order_id)
    {
        $restaurant_id = $id;
        $order_id = $order_id;
        $restaurant = Restaurants::find($restaurant_id);
        $order = MainOrder::find($order_id);
        return view('admin.pages.order_list_details', compact('restaurant_id', 'order_id', 'restaurant', 'order'));
    }
    
    public function alluser_order()
    {
        $order_list = MainOrder::getAllOrders();
        
        if (Auth::User()->usertype!="Admin") {
            \Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');
        }
        
        return view('admin.pages.order_list_for_all', compact('order_list'));
    }

    public function alluser_order_details($order_id)
    {
        $order = MainOrder::find($order_id);
        $restaurants = MainRestaurantOrder::restaurantByMainOrderId($order->id);
        return view('admin.pages.order_list_for_all_details', compact('order', 'restaurants'));
    }

    public function order_status($id, $order_id, $status)
    {
        $order = MainOrder::findOrFail($order_id);
        $order->status = $status;
        $order->save();
        
        \Session::flash('flash_message', 'Status change');

        return \Redirect::back();
    }
     
    public function delete($id, $order_id)
    {
        if (Auth::User()->usertype!="Admin") {
            \Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');
        }
            
        $order = Order::findOrFail($order_id);
        $order->delete();

        \Session::flash('flash_message', 'Deleted');

        return redirect()->back();
    }
    

    public function owner_orderlist()
    {
        $user_id=Auth::User()->id;

        $restaurant= Restaurants::where('user_id', $user_id)->first();

        $restaurant_id=$restaurant['id'];
 
        $order_list = MainOrder::getOwnerOrders($restaurant_id);
        //$order_list = $this->addCommission($order_list);

        if (Auth::User()->usertype!="Owner") {
            \Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');
        }
        
        return view('admin.pages.owner.order_list', compact('order_list', 'restaurant_id'));
    }

    public function owner_orderlist_details($order_id)
    {
        $user_id = Auth::User()->id;
        $restaurant = Restaurants::where('user_id', $user_id)->first();
        $restaurantId = $restaurant->id;

        $order_list = Order::where('restaurant_id', $restaurantId)->where('main_order_id', $order_id)->get();
        $order = Order::ownerOrderlistDetails($restaurantId);

        return view('admin.pages.owner.order_list_details', compact('order_list', 'order', 'restaurantId'));
    }

    public function owner_orderlist_details_update(Request $request)
    {
        $data = \Input::except(array('_token'));
        $inputs = $request->all();
        $mainRestaurantOrder = MainRestaurantOrder::findOrFail($inputs['main_restaurant_order_id']);
        $restaurant = Restaurants::findOrFail($mainRestaurantOrder->restaurant_id);
        $commissionPercentage = $restaurant->commission_percentage;
        $commission = round(($inputs['paid_amount'] * $commissionPercentage) / 100);

        $rule = array(
            'main_restaurant_order_id' => 'required',
            'paid_amount' => 'required|regex:/^\d+(\.\d{1,2})?$/',
        );
   
        $validator = \Validator::make($data, $rule);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->messages());
        }

        if($inputs['paid_amount'] > $mainRestaurantOrder->sub_total){
            return redirect()->back()->withErrors(['message' => 'More than original amount is not acceptable. Please contact to support!']);
        }

        $mainRestaurantOrder->paid_amount = $inputs['paid_amount'];
        $mainRestaurantOrder->paid_commission = $commission;
        $mainRestaurantOrder->save();

        \Session::flash('flash_message', 'Changes Saved');
        return \Redirect::back();
    }

    public function owner_order_status($order_id, $status)
    {
        $order = MainRestaurantOrder::findOrFail($order_id);
        $order->status = $status;
        $order->save();
        
        \Session::flash('flash_message', 'Status change');

        return \Redirect::back();
    }

    private function addCommission($order_list)
    {
        foreach ($order_list as $order) {
            $commissionSlab = CommissionSlab::whereRaw("('$order->sub_total' between min_value and max_value)")->first();
            $order->commission = $commissionSlab->commission;
        }

        return $order_list;
    }

    // public function owner_delete($order_id)
    // {
    //     if (Auth::User()->usertype!="Owner") {
    //         \Session::flash('flash_message', 'Access denied!');

    //         return redirect('admin/dashboard');
    //     }
            
    //     $order = Order::findOrFail($order_id);
    //     $order->delete();

    //     \Session::flash('flash_message', 'Deleted');

    //     return redirect()->back();
    // }
}
