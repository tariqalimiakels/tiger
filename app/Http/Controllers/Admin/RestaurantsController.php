<?php

namespace App\Http\Controllers\Admin;

use App\Area;
use Auth;
use App\User;
use App\Restaurants;
use App\Categories;
use App\Helpers\Helper;
use App\Menu;
use App\Order;
use App\Types;
use App\Review;

use App\Http\Requests;
use App\Location;
use App\RestaurantLocationDelivery;
use Illuminate\Http\Request;
use Session;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class RestaurantsController extends MainAdminController
{
    public function __construct()
    {
        $this->middleware('auth');
          
        parent::__construct();
    }
    public function restaurants()
    {
        $restaurants = Restaurants::orderBy('restaurant_name')->get();
        
        if (Auth::User()->usertype!="Admin") {
            \Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');
        }
         
        return view('admin.pages.restaurants', compact('restaurants'));
    }
    
    public function addeditrestaurant()
    {
        $isEditMode = false;
        $types = Types::orderBy('type')->get();
        $locations = Location::orderBy('location')->get();
        $location_deliveries = [];
        $weekly = $this->weekly();

        return view('admin.pages.addeditrestaurant', compact('types', 'locations', 'location_deliveries', 'weekly', 'isEditMode'));
    }
    
    public function addnew(Request $request)
    { 
        $data =  \Input::except(array('_token'));
        $inputs = $request->all();
        
        $rule=array(
                'restaurant_type' => 'required',
                'restaurant_name' => 'required|unique:restaurants,restaurant_name,'.$inputs['id'],
                'min_order' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'max_order' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'delivery_charge' => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'delivery_time' => 'required',
                'restaurant_address' => 'required',
                'restaurant_logo' => 'mimes:jpg,jpeg,gif,png',
                'restaurant_bg' => 'mimes:jpg,jpeg,gif,png',
                'locations' => 'required|array|min:1',
                 );
        
        $validator = \Validator::make($data, $rule);
 
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->input())->withErrors($validator->messages());
        }
        
        if (!empty($inputs['id'])) {
            $restaurant_obj = Restaurants::findOrFail($inputs['id']);
        } else {
            $restaurant_obj = new Restaurants;
            $restaurant_obj->commission_percentage = 10;
        }

        //Slug
        $restaurant_slug = Str::slug($inputs['restaurant_name'], "-");
        // if ($inputs['restaurant_slug']=="") {
        //     $restaurant_slug = Str::slug($inputs['restaurant_name'], "-");
        // } else {
        //     $restaurant_slug = Str::slug($inputs['restaurant_slug'], "-");
        // }

        //Logo image
        $restaurant_logo = $request->file('restaurant_logo');
        $restaurant_bg = $request->file('restaurant_bg');

        if (!file_exists(public_path() .'/upload/restaurants')) {
            mkdir(public_path() .'/upload/restaurants', 0777, true);
        }
         
        if ($restaurant_logo) {
            \File::delete(public_path() .'/upload/restaurants/'.$restaurant_obj->restaurant_logo.'-b.jpg');
            \File::delete(public_path() .'/upload/restaurants/'.$restaurant_obj->restaurant_logo.'-s.jpg');

            $tmpFilePath = 'upload/restaurants/';
             
            $hardPath = substr($restaurant_slug, 0, 100).'_'.time();
            
            $img = Image::make($restaurant_logo);
            $img->fit(320, 320)->save($tmpFilePath.$hardPath.'-b.jpg');
            $img->fit(98, 98)->save($tmpFilePath.$hardPath. '-s.jpg');

            $restaurant_obj->restaurant_logo = $hardPath;
        }

        if ($restaurant_bg) {

            \File::delete(public_path() .'/upload/restaurants/'.$restaurant_obj->restaurant_bg.'-b.jpg');
            \File::delete(public_path() .'/upload/restaurants/'.$restaurant_obj->restaurant_bg.'-s.jpg');
            
            $tmpFilePath = 'upload/restaurants/';
             
            $hardPathCp = substr($restaurant_slug, 0, 100).'_cover_photo_'.time();

            $img = Image::make($restaurant_bg);
            $img->fit(1800, 1200)->save($tmpFilePath.$hardPathCp.'-b.jpg');
            $img->fit(98, 98)->save($tmpFilePath.$hardPathCp. '-s.jpg');

            $restaurant_obj->restaurant_bg = $hardPathCp;
        }
        
        if (Auth::User()->usertype == "Owner") {
            $restaurant_obj->user_id = Auth::User()->id;
        }else if (Auth::User()->usertype == "Admin"){
            $restaurant_obj->commission_percentage = $inputs['commission_percentage'];
        }
        $restaurant_obj->restaurant_type = $inputs['restaurant_type'];
        $restaurant_obj->restaurant_name = $inputs['restaurant_name'];
        $restaurant_obj->restaurant_slug = $restaurant_slug;
        $restaurant_obj->min_order = $inputs['min_order'];
        $restaurant_obj->max_order = $inputs['max_order'];
        $restaurant_obj->delivery_charge = $inputs['delivery_charge'];
        $restaurant_obj->delivery_time = $inputs['delivery_time'];
        $restaurant_obj->restaurant_address = $inputs['restaurant_address'];
        $restaurant_obj->restaurant_description = "";

        //opening timings
        foreach ($this->weekly() as $daily) {
            foreach ($daily as $daily_open_close) {
                $restaurant_obj->$daily_open_close = ($inputs[$daily_open_close] != "") ? $inputs[$daily_open_close] : null;
            }
        }
        
        $restaurant_obj->save();

        RestaurantLocationDelivery::where('restaurant_id', $restaurant_obj->id)->delete();
        foreach ($inputs['locations'] as $location) {
            $restaurantDelivery = new RestaurantLocationDelivery;
            $restaurantDelivery->restaurant_id = $restaurant_obj->id;
            $restaurantDelivery->location_id = $location;
            $restaurantDelivery->delivery_charges = isset($inputs['delivery_charges'][$location]) ? $inputs['delivery_charges'][$location] : null;
            $restaurantDelivery->delivery_time = isset($inputs['delivery_times'][$location]) ? $inputs['delivery_times'][$location] : null;
            $restaurantDelivery->save();
        }
        
        if (Auth::User()->usertype == "Owner") {
            \Session::flash('flash_message', 'Restaurant successfully added.');
            if (!empty($inputs['id'])) {
                \Session::flash('flash_message', 'Restaurant successfully updated.');
            }
            session(['restaurant_id' => $restaurant_obj->id]);
            return redirect('admin/myrestaurants/'.$restaurant_obj->id);
        }

        if (!empty($inputs['id'])) {
            \Session::flash('flash_message', 'Changes Saved');
            return \Redirect::back();
        } else {
            \Session::flash('flash_message', 'Added');
            return \Redirect::back();
        }
    }
    
    public function editrestaurant($id)
    { 
        $isEditMode = true;
        if($id == 0){
            return redirect('admin/restaurants/addrestaurant');
        }
        $types = Types::orderBy('type')->get();
        $restaurant= Restaurants::findOrFail($id);
        $locations = Location::orderBy('location')->get();
        $locationsDeliveries = RestaurantLocationDelivery::where('restaurant_id', $id)->get();
        $location_deliveries = $locationsDeliveries->pluck('location_id')->toArray();
        foreach($locationsDeliveries as $locationsDelivery){
            $location_delivery_charges[$locationsDelivery->location_id] = $locationsDelivery->delivery_charges;
            $location_delivery_times[$locationsDelivery->location_id] = $locationsDelivery->delivery_time;
        }
        $weekly = $this->weekly();
        //dd($location_delivery_times);
        return view('admin.pages.addeditrestaurant', compact('restaurant', 'types', 'locations', 'location_deliveries', 'location_delivery_charges', 'location_delivery_times', 'weekly', 'isEditMode'));
    }
    
    public function delete($id)
    {
        if (Auth::User()->usertype!="Admin") {
            \Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');
        }
            
        $cat = Restaurants::findOrFail($id);
        $cat->delete();

        \Session::flash('flash_message', 'Deleted');

        return redirect()->back();
    }

    public function restaurantview($id)
    {
        if (Auth::User()->usertype!="Admin") {
            \Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');
        }

           
        $restaurant= Restaurants::findOrFail($id);
          
        $categories_count = Categories::where(['restaurant_id' => $id])->count();

        $menu_count = Menu::where(['restaurant_id' => $id])->count();

        $order_count = Order::where(['restaurant_id' => $id])->count();

        $review_count = Review::where(['restaurant_id' => $id])->count();

        return view('admin.pages.restaurantview', compact('restaurant', 'categories_count', 'menu_count', 'order_count', 'review_count'));
    }
    
    public function reviewlist($id)
    {
        $review_list = Review::where("restaurant_id", $id)->orderBy('date')->get();
        
        if (Auth::User()->usertype!="Admin") {
            \Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');
        }
        
        $restaurant_id=$id;
 

        return view('admin.pages.review_list', compact('review_list', 'restaurant_id'));
    }


    public function my_restaurants()
    {
        if (Auth::User()->usertype!="Owner") {
            \Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');
        }

        $user_id=Auth::User()->id;

        $restaurant= Restaurants::where('user_id', $user_id)->first();
         
        $types = Types::orderBy('type')->get();

        /* $restaurant= Restaurants::findOrFail($id);

         $categories_count = Categories::where(['restaurant_id' => $id])->count();

         $menu_count = Menu::where(['restaurant_id' => $id])->count();

         $order_count = Order::where(['restaurant_id' => $id])->count();

         $review_count = Review::where(['restaurant_id' => $id])->count();

         return view('admin.pages.restaurantview',compact('restaurant','categories_count','menu_count','order_count','review_count'));*/

        return view('admin.pages.owner_restaurantview', compact('restaurant', 'types'));
    }

    public function owner_reviewlist()
    {
        if (Auth::User()->usertype!="Owner") {
            \Session::flash('flash_message', 'Access denied!');

            return redirect('admin/dashboard');
        }
        

        $user_id=Auth::User()->id;

        $restaurant= Restaurants::where('user_id', $user_id)->first();

        $restaurant_id=$restaurant['id'];

        $review_list = Review::where("restaurant_id", $restaurant_id)->orderBy('date')->get();
       

        return view('admin.pages.owner.review_list', compact('review_list', 'restaurant_id'));
    }
}
