<?php

namespace App\Http\Controllers\Admin;

use App\CommissionSlab;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommissionsController extends Controller
{
    public function commission_slabs_list(){
        $commissionSlabs = CommissionSlab::orderBy('min_value', 'asc')->get();
        return view('admin.pages.owner.commission_slabs_list', compact('commissionSlabs'));
    }
}
