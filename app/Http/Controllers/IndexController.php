<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Restaurants;
use App\Categories;
use App\Helpers\Helper;
use App\Menu;
use App\Types;
use App\Review;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function index()
    {
        $types=Types::orderBy('type')->get();
        $todayNameColumns = Helper::todayNameColumns();
        $currentTime = Helper::getBufferTime();
        $openColumn = $todayNameColumns['openColumn'];
        $closeColumn = $todayNameColumns['closeColumn'];

        //Check time and free restaurants
        $restaurants = Restaurants::getPopularRestaurants($currentTime, $openColumn, $closeColumn);

        if (count($restaurants) == 0) {
             
            //Check time and if there is no free restaurants then restaurants that completed their orders
            $restaurants = Restaurants::getPopularRestaurants($currentTime, $openColumn, $closeColumn, true, true, 'Completed');
            

            if (count($restaurants) == 0) {
                //Check time only
                $restaurants = Restaurants::getPopularRestaurants($currentTime, $openColumn, $closeColumn, true, false);
 
                //without checking anything
                if (count($restaurants) == 0) {
                    $restaurants = Restaurants::getPopularRestaurants($currentTime, $openColumn, $closeColumn, false, false);
                }
            }
        }

        $restaurants = $this->assignCategoriesNames($restaurants);
        $restaurants = $this->assignDeliveryChargesFrom($restaurants);

        return view('pages.index', compact('restaurants', 'types'));
    }
    
    public function about_us()
    {
        return view('pages.about');
    }

    public function contact_us()
    {
        return view('pages.contact');
    }

    /**
     * If application is already installed.
     *
     * @return bool
     */
    public function alreadyInstalled()
    {
        return file_exists(storage_path('installed'));
    }

    /**
     * Do user login
     * @return $this|\Illuminate\Http\RedirectResponse
     */
     
    public function login()
    {
        return view('pages.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, $request->has('remember'))) {
            if (Auth::user()->usertype=='banned') {
                \Auth::logout();
                return array("errors" => 'You account has been banned!');
            }

            if (Auth::user()->usertype!='User') {
                return redirect('/admin');
            }

            return redirect('/');
            //return $this->handleUserWasAuthenticated($request);
        }

        return redirect('/login')->withErrors('The email or the password is invalid. Please try again.');
    }
    
    /**
    * Send the response after the user was authenticated.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  bool  $throttles
    * @return \Illuminate\Http\Response
    */
    protected function handleUserWasAuthenticated(Request $request)
    {
        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::user());
        }

        return redirect('/');
    }
    
    
    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        $userType = Auth::user()->user_type;
        Auth::logout();
        \Session::flash('flash_message', 'Logout successfully...');

        return redirect('/');
    }


    public function register()
    {
        return view('pages.register');
    }

    public function profile()
    {
        $user_id=Auth::user()->id;
        $user = User::findOrFail($user_id);

        return view('pages.profile', compact('user'));
    }
    

    public function editprofile(Request $request)
    {
        $data =  \Input::except(array('_token')) ;
        
        $inputs = $request->all();
        
        
        $rule=array(
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'phone' => ['required', 'string', 'min:11'],
        );
       
        
        $validator = \Validator::make($data, $rule);
 
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->messages());
        }
          
        $user_id=Auth::user()->id;
           
        $user = User::findOrFail($user_id);
        $user->name = $inputs['name'];
        $user->email = $inputs['email'];
        $user->mobile = $inputs['phone'];
        $user->save();
        
         
        \Session::flash('flash_message', 'Your account has been updated.');

        return \Redirect::back();
    }

    public function change_password()
    {
        return view('pages.change_password');
    }

        
    public function edit_password(Request $request)
    {
        $data =  \Input::except(array('_token')) ;
        
        $inputs = $request->all();
        
        $rule=array(
            'password' => ['required', 'string', 'min:8', 'confirmed'],
                 );
        
        
        
        $validator = \Validator::make($data, $rule);
 
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->messages());
        }
          
       
        $user_id=Auth::user()->id;
           
        $user = User::findOrFail($user_id);
       
        $user->password= bcrypt($inputs['password']);
        
         
        $user->save();

        \Session::flash('flash_message', 'Password has been changed successfully.');

        return \Redirect::back();
    }

    public function contact_send(Request $request)
    {
        $data = \Input::except(array('_token')) ;
        
        $inputs = $request->all();
        
        $rule = array(
                'name' => 'required',
                'email' => 'required|email|max:75',
                'phone' => ['required', 'string', 'digits:11', 'regex:/(03)[0-9]{9}/'],
                'message' => 'required',
                 );
        
        $validator = \Validator::make($data, $rule);
 
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->messages());
        }
          
        $data = array(
            'name' => $inputs['name'],
            'email' => $inputs['email'],
            'phone' => $inputs['phone'],
            'user_message' => $inputs['message'],
             );

        \Mail::send('emails.contact', $data, function ($message) {
            $message->from(config('settings.site_email'), config('settings.site_name'));
            $message->to(config('settings.site_email'))->subject('Contact Form');
        });
        
        \Session::flash('flash_message', 'Thank You. Your Message has been Submitted.');
        return \Redirect::back();
    }
}
