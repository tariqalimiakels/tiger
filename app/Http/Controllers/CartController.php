<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Restaurants;
use App\Cart;
use App\Categories;
use App\City;
use App\CommissionSlab;
use App\Helpers\Helper;
use App\Order;
use App\Menu;
use App\Types;

use App\Http\Requests;
use App\Location;
use App\MainOrder;
use App\MainRestaurantOrder;
use App\RestaurantLocationDelivery;
use Illuminate\Http\Request;
use Session;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class CartController extends Controller
{
    public function add_cart_item($item_id, $type = 'add')
    {
        $user_id = Auth::user()->id;
        $menu = Menu::findOrFail($item_id);

        if(session('location')){
            $locationId = session('location');
            $isRestaurantCanDeliver = RestaurantLocationDelivery::where('restaurant_id', $menu->restaurant_id)->where('location_id', $locationId)->first();
            
            if ($isRestaurantCanDeliver == "") {
                return redirect()->back()->withErrors(['message' => 'This restaurant can not be delivered at your location. Please choose any other restaurant.']);
            }
        }else{
            return redirect()->back()->withErrors(['message' => 'Set the delivery location first!']);
        }

        if ($type == 'add') {
            $subTotal = DB::table('cart')->where('user_id', Auth::id())->where('restaurant_id', $menu->restaurant_id)->sum('item_price');
            $restaurant = Restaurants::find($menu->restaurant_id);
            $totalCartPriceWithNewItem = $subTotal + $menu->price;
            if ($totalCartPriceWithNewItem > $restaurant->max_order) {
                return redirect()->back()->withErrors(['message' => 'Amount exceeded. Maximum amount for this restaurant is ' . Helper::getFormattedPrice($restaurant->max_order) . '.']);
            }
        }

        $find_cart_item = Cart::where(['user_id'=>$user_id,'item_id'=>$item_id])->first();
        if ($find_cart_item!="") {
            $singl_item_price = $find_cart_item->item_price/$find_cart_item->quantity;

            if ($type == 'add') {
                $find_cart_item->increment('quantity');
            } else {
                if ($find_cart_item->quantity > 1) {
                    $find_cart_item->decrement('quantity');
                }
            }

            $new_quantity = $find_cart_item->quantity;
            $new_price = $singl_item_price*$new_quantity;
            $find_cart_item->item_price=$new_price;
            $find_cart_item->save();
        } else {

            $cart = new Cart;
            $cart->user_id = $user_id;
            $cart->restaurant_id = $menu->restaurant_id;
            $cart->item_id = $menu->id;
            $cart->item_name = $menu->menu_name;
            $cart->original_price = $menu->price;
            $cart->item_price = $menu->price;
            $cart->quantity= '1';
           
            $cart->save();
        }
              
        return \Redirect::back()->with('is_item_added', true);
    }
    
    public function delete_cart_item($id)
    {
        $cart = Cart::findOrFail($id);
          
        $cart->delete();
         
        return redirect()->back();
    }

    public function cart_details()
    {
        $locationId = session('location');
        $location = Location::find($locationId);
        return view('pages.cart');
    }
     
    public function order_details()
    {
        $user_id=Auth::user()->id;
        $user = User::findOrFail($user_id);

        return view('pages.cart_order_details', compact('user'));
    }

    public function confirm_order_details(Request $request)
    {
        $now = Carbon::now();
        $todayName = $now->dayName; //Monday, Tuesday
        $currentTime = $now->format('H:i');
        $data =  \Input::except(array('_token'));
        $locationId = session('location');
        $todayNameColumns = Helper::todayNameColumns();
        $currentTime = Helper::getBufferTime();
        $openColumn = $todayNameColumns['openColumn'];
        $closeColumn = $todayNameColumns['closeColumn'];
        $isDelivered = true;
        
        $inputs = $request->all();

        $rule=array(
                'mobile' => ['required', 'string', 'digits:11', 'regex:/(03)[0-9]{9}/'],
                'address' => 'required',
                'location' => 'required',
            );
       
        $validator = \Validator::make($data, $rule);
 
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->messages());
        }

        $user_id=Auth::user()->id;
        $cartRestaurants = Cart::getCartUniqueRestaurants($user_id);
        $city = City::first();

        //check if restaurant can deliver or not
        foreach ($cartRestaurants as $restaurant) {
            // $todayNameColumns = $this->weekly()[$todayName];
            // $openColumn = $todayNameColumns[0];
            // $closeColumn = $todayNameColumns[1];

            // $currentTime = strtotime($currentTime);
            $startTime = strtotime($restaurant->$openColumn);
            $endTime = strtotime($restaurant->$closeColumn);

            //check if restaurant can deliver the given areas
            $isRestaurantCanDeliver = RestaurantLocationDelivery::where('restaurant_id', $restaurant->restaurant_id)->where('location_id', $locationId)->first();
            if (!$isRestaurantCanDeliver) {
                $message = $restaurant->restaurant_name . " cannot be deliver at your location. Please choose any other restaurant.";
                return redirect()->back()->withInput($request->input())->withErrors(['message' => $message]);
            }

            if ($startTime == false || $endTime == false) {
                $message = $restaurant->restaurant_name . " is closed today.";
                $isDelivered = false;
            } else {
                //check if time in between
                $isOpenRestaurant = Restaurants::where('id', $restaurant->restaurant_id)->whereRaw("('$currentTime' between restaurants.`$openColumn` and restaurants.`$closeColumn`)")->first();
                if (!$isOpenRestaurant) {
                    $message = $restaurant->restaurant_name . " is closed. The timings are " . Helper::convertTimeToTweHoursFormat($restaurant->$openColumn) . " - " . Helper::convertTimeToTweHoursFormat($restaurant->$closeColumn) . " for " . $todayName . ".";
                    $isDelivered = false;
                }
            }

            if(!$isDelivered){
                return redirect()->back()->withInput($request->input())->withErrors(['message' => $message]);
            }

            //check if time in between
            // if (
            //     ($startTime < $endTime && $currentTime >= $startTime && $currentTime <= $endTime) ||
            //     ($startTime > $endTime && ($currentTime >= $startTime || $currentTime <= $endTime))
            // ) {
            //     //open
            // } else {

            //     if($startTime == false || $endTime == false){
            //         $message = $restaurant->restaurant_name . " is closed today.";
            //     }else{
            //         $message = $restaurant->restaurant_name . " is closed. The timings are " . Helper::convertTimeToTweHoursFormat($restaurant->$openColumn) . " - " . Helper::convertTimeToTweHoursFormat($restaurant->$closeColumn) . " for " . $todayName . ".";
            //     }
            //     return redirect()->back()->withInput($request->input())->withErrors(['message' => $message]);
            // }
        }

         //check if each category is available or not
        $cartData = Cart::getCart($user_id);
        foreach($cartData as $cart)
        {
            $category = Categories::getCategoryByMenu($cart->item_id);
            $startTime = strtotime($category->$openColumn);
            $endTime = strtotime($category->$closeColumn);

            if ($startTime != false || $endTime != false) {
                $isCategoryAvailbleNow = Categories::checkIfCategoryAvailbleNow($cart->item_id, $openColumn, $closeColumn);
                if (!$isCategoryAvailbleNow) {
                    $message = $category->menu_name . " is not available. The timings are " . Helper::convertTimeToTweHoursFormat($category->$openColumn) . " - " . Helper::convertTimeToTweHoursFormat($category->$closeColumn) . " for " . $todayName . ".";
                    return redirect()->back()->withInput($request->input())->withErrors(['message' => $message]);
                }
            }
            
        }
        
           
        $user = User::findOrFail($user_id);
        $user->mobile = $inputs['mobile'];
        $user->address = $inputs['address'];
        $user->save();

        $mainOrder = new MainOrder;
        $mainOrder->user_id = $user_id;
        $mainOrder->city_id = $city->id;
        $mainOrder->location_id = $locationId;
        $mainOrder->phone = $inputs['mobile'];
        $mainOrder->address = $inputs['address'];
        $mainOrder->sub_total = Cart::where('user_id', $user_id)->sum('item_price');
        $mainOrder->delivery_charges = Cart::getDeliveryCharges($user_id);
        $mainOrder->total_amount = $mainOrder->sub_total + $mainOrder->delivery_charges;
        $mainOrder->status= 'Pending';
        $mainOrder->save();

        $mainOrderId = $mainOrder->id;

        foreach ($cartRestaurants as $restaurant) {
            $restaurantId = $restaurant->restaurant_id;
            $commissionPercentage = $restaurant->commission_percentage;
            $subTotal = Cart::where('user_id', $user_id)->where('restaurant_id', $restaurantId)->sum('item_price');
            // $commissionSlab = CommissionSlab::whereRaw("('$subTotal' between min_value and max_value)")->first();
            $commission = round(($subTotal * $commissionPercentage) / 100);

            $mainRestaurantOrder = new MainRestaurantOrder;
            $mainRestaurantOrder->main_order_id = $mainOrderId;
            $mainRestaurantOrder->restaurant_id = $restaurantId;
            $mainRestaurantOrder->sub_total = $subTotal;
            $mainRestaurantOrder->delivery_charges = Cart::calculateDeliverCharges($restaurantId, $locationId, $restaurant->delivery_charge);
            $mainRestaurantOrder->total_amount = $mainRestaurantOrder->sub_total + $mainRestaurantOrder->delivery_charges;
            $mainRestaurantOrder->commission = $commission;
            $mainRestaurantOrder->comment = $inputs['comment'][$restaurantId];
            $mainRestaurantOrder->status= 'Pending';
            $mainRestaurantOrder->save();
        }

        $cart_res=Cart::where('user_id', $user_id)->orderBy('id')->get();
        foreach ($cart_res as $cart_item) {
            $restaurant = Restaurants::find($cart_item->restaurant_id);

            $order = new Order;
            $order->user_id = $user_id;
            $order->main_order_id = $mainOrderId;
            $order->restaurant_id =$cart_item->restaurant_id;
            $order->item_id = $cart_item->item_id;
            $order->item_name = $cart_item->item_name;
            $order->original_price = $cart_item->original_price;
            $order->item_price = $cart_item->item_price;
            $order->quantity= $cart_item->quantity;
            $order->save();
        }

        
        if (false) {
            foreach ($cartRestaurants as $restaurant) {
                $restaurantData = Restaurants::findOrFail($restaurant->restaurant_id);

                //$order_list = Order::where(array('user_id'=>$user_id,'status'=>'Pending'))->orderBy('item_name','desc')->get();
                $order_list = Cart::where('user_id', $user_id)->where('restaurant_id', $restaurant->restaurant_id)->orderBy('id')->get();

                $data = array(
                    'name' => $inputs['name'],
                    'order_list' => $order_list
                );

                $subject=getcong('site_name').' Order Confirmed';
            
                $user_order_email=$inputs['email'];

                //User Email

                \Mail::send('emails.order_item', $data, function ($message) use ($subject,$user_order_email) {
                    $message->from(getcong('site_email'), getcong('site_name'));

                    $message->to($user_order_email)->subject($subject);
                });
            

                //Owner Email
                $subject2='New Order Placed';
                $owner_admin_order_email=$restaurantData->email;

                \Mail::send('emails.order_item_owner_admin', $data, function ($message) use ($subject2,$owner_admin_order_email) {
                    $message->from(getcong('site_email'), getcong('site_name'));

                    $message->to($owner_admin_order_email)->subject($subject2);
                });

                //Admin
                $owner_admin_order_email=getcong('site_email');
                \Mail::send('emails.order_item_owner_admin', $data, function ($message) use ($subject2,$owner_admin_order_email) {
                    $message->from(getcong('site_email'), getcong('site_name'));

                    $message->to($owner_admin_order_email)->subject($subject2);
                });
            }
        }
        
        Cart::where('user_id', $user_id)->delete();
        return redirect('orders');
    }

    public function user_orderlist()
    {
        $user_id = Auth::user()->id;
        $order_list = DB::table('main_orders')
                ->select('main_orders.*', 'locations.location')
                ->join('locations', 'locations.id', '=', 'main_orders.location_id')
                ->where('main_orders.user_id', $user_id)->orderBy('id', 'desc')->get();

        foreach($order_list as $orderList){
            $order = DB::table('main_restaurant_orders')->where('main_order_id', $orderList->id)->get();
            $isAnyPending = $order->filter(function ($value, $key) {
                return $value->status == 'Pending';
            });

            $isAnyConfirmed = $order->filter(function ($value, $key) {
                return $value->status == 'Confirmed';
            });

            $isAnyProcessing = $order->filter(function ($value, $key) {
                return $value->status == 'Processing';
            });

            if(count($isAnyPending) > 0){
                $orderList->status = 'Pending';
                continue;
            }

            if(count($isAnyConfirmed) > 0){
                $orderList->status = 'Confirmed';
                continue;
            }

            if(count($isAnyProcessing) > 0){
                $orderList->status = 'Processing';
                continue;
            }

            $orderList->status = 'Completed';
        }

        return view('pages.my_order', compact('order_list'));
    }

    public function user_orderdetails($order_id)
    {
        $order = MainOrder::findOrFail($order_id);
        $restaurants = MainRestaurantOrder::restaurantByMainOrderId($order->id);
        return view('pages.my_order_details', compact('order', 'restaurants'));
    }

    public function cancel_order($order_id)
    {
        $order = Order::findOrFail($order_id);
        

        $order->status = 'Cancel';
        $order->save();
        
        
        \Session::flash('flash_message', 'Order has been cancel');

        return \Redirect::back();
    }
}
