<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Restaurants;
use App\Categories;
use App\Helpers\Helper;
use App\Menu;
use App\Types;
use App\Review;

use App\Http\Requests;
use App\Location;
use App\RestaurantLocationDelivery;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Session;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;

class RestaurantsController extends Controller
{
    public function index(Request $request)
    {
        $restaurants = DB::table('restaurants')
                           ->leftJoin('restaurant_types', 'restaurants.restaurant_type', '=', 'restaurant_types.id')
                           ->select('restaurants.*', 'restaurant_types.type')
                           //->where('restaurants.cat_id', '=', $cat->id)
                           ->orderBy('id', 'desc')
                           ->paginate(10);
        
        $restaurants->setPath($request->url());

        return view('pages.restaurants', compact('restaurants'));
    }

    public function restaurants_type(Request $request, $type)
    {
        $location = session('location', "");
        $description = "";
        $title = "Restaurants Type " . ucwords($type);
        $todayNameColumns = Helper::todayNameColumns();
        $currentTime = Helper::getBufferTime();
        $openColumn = $todayNameColumns['openColumn'];
        $closeColumn = $todayNameColumns['closeColumn'];
        $headTitle = ucwords($type) . ' delivery from the best restaurants' .' | '. config('settings.site_name');

        if ($location != "") {
            $locationData = Location::find($location);
            $description = "Restaurants near " . $locationData->location;
        }

        //Check time and free restaurants
        $restaurants = Restaurants::getByType($type, $currentTime, $openColumn, $closeColumn);
         
        if (count($restaurants) == 0) {
             
              //Check time and if there is no free restaurants then restaurants that completed their orders
            $restaurants = Restaurants::getByType($type, $currentTime, $openColumn, $closeColumn, true, true, 'Completed');

            if (count($restaurants) == 0) {
                //Check time only
                $restaurants = Restaurants::getByType($type, $currentTime, $openColumn, $closeColumn, true, false);
 
                //without checking anything
                if (count($restaurants) == 0) {
                    $restaurants = Restaurants::getByType($type, $currentTime, $openColumn, $closeColumn, false, false);
                }
            }
        }
        
        $restaurants = $this->assignCategoriesNames($restaurants);
        $restaurants = $this->assignDeliveryChargesFrom($restaurants);
        $restaurants->setPath($request->url());
         
        return view('pages.restaurants', compact('restaurants', 'title', 'description', 'type', 'headTitle'));
    }

    public function restaurants_search(Request $request)
    {
        $inputs = $request->all();
        $keyword = $request->has('search_keyword') ? $inputs['search_keyword'] : session('search_keyword');
        $locationId = $request->has('location') ? $inputs['location'] : session('location');
        $title = 'Restaurants';
        $todayNameColumns = Helper::todayNameColumns();
        $currentTime = Helper::getBufferTime();
        $openColumn = $todayNameColumns['openColumn'];
        $closeColumn = $todayNameColumns['closeColumn'];
        session(['location' => $locationId, 'keyword' => $keyword]);

        if ($locationId != "") {
            $locationData = Location::find($locationId);
            $description = "Restaurants near " . $locationData->location;

            if($request->has('header_search')){
                return redirect()->back();
            }
        }
        if ($keyword != "") {
            $description .= ' and keyword <strong>' . $keyword . '</strong>';
        }

        //Check time and free restaurants
        $restaurants = Restaurants::searchByKeyword($keyword, $currentTime, $openColumn, $closeColumn);

        if (count($restaurants) == 0) {
             
            //Check time and if there is no free restaurants then restaurants that completed their orders
            $restaurants = Restaurants::searchByKeyword($keyword, $currentTime, $openColumn, $closeColumn, true, true, 'Completed');

            if (count($restaurants) == 0) {
                //Check time only
                $restaurants = Restaurants::searchByKeyword($keyword, $currentTime, $openColumn, $closeColumn, true, false);
 
                //without checking anything
                if (count($restaurants) == 0) {

                    $restaurants = Restaurants::searchByKeyword($keyword, $currentTime, $openColumn, $closeColumn, false, false);
                }
            }
        }
       
        $restaurants = $this->assignCategoriesNames($restaurants);
        $restaurants = $this->assignDeliveryChargesFrom($restaurants);

        $headTitle = "Best restaurant near {$locationData->location}";
        if($keyword != ""){

            $headTitle = "Get {$keyword} delivered from the best restaurants" ;
        }
        $headTitle .= ' | '. config('settings.site_name');
         
        return view('pages.restaurants', compact('restaurants', 'title', 'description', 'headTitle'));
    }
     
    public function restaurants_menu($slug)
    {
        $restaurant = DB::table('restaurants')
                    ->select('restaurants.*')
                    ->join('users', 'users.id', '=', 'restaurants.user_id')
                    ->where("users.status", User::STATUS_ACTIVE)
                    ->where("restaurants.restaurant_slug", $slug)
                    ->first();

        if ($restaurant === null){
            throw new ModelNotFoundException('Restaurant not found');
        }

        $deliveryLocations = DB::table('locations')
            ->select('locations.location')
            ->join('restaurant_location_deliveries', 'restaurant_location_deliveries.location_id', '=', 'locations.id')
            ->join('restaurants', 'restaurants.id', '=', 'restaurant_location_deliveries.restaurant_id')
            ->where('restaurant_id',  $restaurant->id)->limit(10)->get();
        $categories = Categories::inRandomOrder()->where("restaurant_id", $restaurant->id)->limit(8)->get();
        $title = '<a href="javascript:void(0)" class="restaurant-info">'.Helper::limitCharacters($restaurant->restaurant_name, 20).'</a>';
        $weekly = $this->weekly();
        return view('pages.restaurants_menu', compact('restaurant', 'title', 'weekly', 'categories', 'deliveryLocations'));
    }

    public function restaurants_check_delivery(Request $request)
    {
        $inputs = $request->all();
        $isDeliver = false;
        session(['location' => $inputs['location_id']]);
        $isRestaurantCanDeliver = RestaurantLocationDelivery::where('restaurant_id', $inputs['restaurant_id'])->where('location_id', $inputs['location_id'])->first();

        if ($isRestaurantCanDeliver) {
            $isDeliver = true;
        }

        return response()->json([
            'success' => true,
            'data' => ['is_deliver' => $isDeliver]
        ]);
    }
    
    public function restaurants_details($slug, Request $request)
    {
        $restaurant = Restaurants::where("restaurant_slug", $slug)->first();
          
        $reviews = DB::table('restaurant_review')
                           ->select('restaurant_review.*')
                           ->where('restaurant_review.restaurant_id', '=', $restaurant->id)
                           ->orderBy('restaurant_review.id', 'desc')
                           ->paginate(10);
        
        $reviews->setPath($request->url());

        $total_review = Review::where("restaurant_id", $restaurant->id)->count();
          
        return view('pages.restaurants_details', compact('restaurant', 'reviews', 'total_review'));
    }

    public function restaurants_rating(Request $request, $rating)
    {
        $restaurants = DB::table('restaurants')
                           ->leftJoin('restaurant_types', 'restaurants.restaurant_type', '=', 'restaurant_types.id')
                           ->select('restaurants.*', 'restaurant_types.type')
                           ->where('restaurants.review_avg', '=', $rating)
                           ->orderBy('id', 'desc')
                           ->paginate(10);
        
        $restaurants->setPath($request->url());

         
         
        return view('pages.restaurants', compact('restaurants'));
    }
    
    public function restaurant_review(Request $request)
    {
        $inputs = $request->all();

        $user_id=Auth::user()->id;

        $review = new Review;

        $review->restaurant_id = $inputs['restaurant_id'];
        $review->user_id = $user_id;
        $review->review_text = $inputs['review_text'];
        $review->food_quality = $inputs['food_quality'];
        $review->price = $inputs['price'];
        $review->punctuality = $inputs['punctuality'];
        $review->courtesy = $inputs['courtesy'];
        $review->date= strtotime(date('Y-m-d'));

        $review->save();

        $food_quality=round(DB::table('restaurant_review')->where('restaurant_id', $inputs['restaurant_id'])->avg('food_quality'));

        $price=round(DB::table('restaurant_review')->where('restaurant_id', $inputs['restaurant_id'])->avg('price'));

        $punctuality=round(DB::table('restaurant_review')->where('restaurant_id', $inputs['restaurant_id'])->avg('punctuality'));

        $courtesy=round(DB::table('restaurant_review')->where('restaurant_id', $inputs['restaurant_id'])->avg('courtesy'));

        $total_avg=round($food_quality+$price+$punctuality+$courtesy)/4;

        $restaurant_obj = Restaurants::findOrFail($inputs['restaurant_id']);

        $restaurant_obj->review_avg = $total_avg;
        $restaurant_obj->save();

        return \Redirect::back();
    }
}
