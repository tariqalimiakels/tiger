<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Restaurants;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    public $redirectToPrevious = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectToPrevious = url()->previous();
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        if (session('link')) {
            $myPath     = session('link');
            $loginPath  = url('/login');
            $previous   = url()->previous();

            if ($previous = $loginPath) {
                session(['link' => $myPath]);
            } else {
                session(['link' => $previous]);
            }
        } else {
            session(['link' => url()->previous()]);
        }

        return view('auth.login');
    }

    protected function authenticated(Request $request, $user)
    {
        if ($user->usertype == 'Owner') {
            $restaurant = Restaurants::where('user_id', $user->id)->first();
            $restaurantId = $restaurant ? $restaurant->id : 0;
            session(['restaurant_id' => $restaurantId]);
        }

        if ($user->usertype != 'User') {
            return redirect('/admin');
        }

        return redirect(session('link'));
    }

    protected function loggedOut(Request $request)
    {
        session()->forget(['restaurant_id']);
    }
}
