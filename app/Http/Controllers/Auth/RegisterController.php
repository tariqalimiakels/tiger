<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required', 'string', 'digits:11', 'regex:/(03)[0-9]{9}/'],
            'usertype' => ['required', 'string'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'mobile' => $data['phone'],
            'usertype' => $data['usertype'],
        ]);
    }

    public function register(Request $request)
    {
       
        $this->validator($request->all())->validate();

        if ($request->usertype == 'Owner') {
            if (!$request->has('verification_code')) {
                $fourRandomDigit = mt_rand(10000, 99999);
                session(['verification_code' =>  $fourRandomDigit]);
                return redirect()->back()
                    ->withInput($request->input())
                    ->with('status', 'Verification code has been sent to you by SMS. This verification code is valid for the next 10 minutes.');
            } else {

                if ($request->verification_code != session('verification_code')) {
                    return redirect()->back()
                    ->withInput($request->input())
                    ->with('status', 'Verification code is not correct.');
                }
            }
        }

        session()->forget(['verification_code']);
        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }
}
