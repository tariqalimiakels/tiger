<?php

namespace App\Helpers;

use App\Http\Controllers\Controller;
use App\Location;
use Illuminate\Support\Carbon;

class Helper
{
    public static function getFormattedPrice($price)
    {
        return config('settings.currency_symbol') . ' ' . number_format($price, 2);
    }

    public static function classActivePath($path)
    {
        $path = explode('.', $path);
        $segment = 2;
        foreach ($path as $p) {
            if ((request()->segment($segment) == $p) == false) {
                return '';
            }
            $segment++;
        }
        return ' active';
    }

    public static function convertTimeToTweHoursFormat($time)
    {
        return date("g:i A", strtotime($time));
    }

    public static function limitCharacters($string, $limit = 30)
    {
        $separator = (strlen($string) > $limit) ? '...' : '';
        return substr($string, 0, $limit) . $separator;
    }

    public static function readableDateFormat($date)
    {
        return Carbon::parse($date)->format('d M, Y h:m A');
    }

    public static function restaurantTodaysTimings($restaurant, $returnBoolean = false)
    {
        $todayNameColumns = static::todayNameColumns();
        $start = $todayNameColumns['openColumn'];
        $end = $todayNameColumns['closeColumn'];
        $openTime = $restaurant->$start;
        $closeTime = $restaurant->$end;
        $isOpen = false;

        if ($openTime == "" || $closeTime == "") {
            return '<span class="res_mimimum closed-text">Closed - Today</span>';
        }

        $isOpen = Carbon::now()->addMinutes(config('constants.buffer_time_restaurant'))->between(
            Carbon::parse($openTime),
            Carbon::parse($closeTime)
        );

        if ($isOpen) {
            $timing = '<span class="res_mimimum open-text">Open now</span>';
            $isOpen = true;
        } else {
            $timing = '<span class="res_mimimum closed-text">Closed now</span>';
        }

        $timing = $timing . '<span class="res_mimimum">'. static::readableTimeFormat($openTime).' - ' .static::readableTimeFormat($closeTime).'</span>';
        return $returnBoolean ? $isOpen : $timing;
    }

    public static function categoryTodaysTimings($category, $returnBoolean = false)
    {
        $todayNameColumns = static::todayNameColumns();
        $start = $todayNameColumns['openColumn'];
        $end = $todayNameColumns['closeColumn'];
        $openTime = $category->$start;
        $closeTime = $category->$end;
        $isOpen = static::restaurantTodaysTimings($category, true);
        $timing = '';

        if (!$isOpen) {
            $timing = '<span class="res_mimimum closed-text">Not available</span>';
            $timing = $timing . '<span class="res_mimimum">'. static::readableTimeFormat($openTime).' - ' .static::readableTimeFormat($closeTime).'</span>';
        }

        return $returnBoolean ? $isOpen : $timing;
    }

    public static function weekly()
    {
        return [
                'Monday' => ['open_monday', 'close_monday'],
                'Tuesday' => ['open_tuesday', 'close_tuesday'],
                'Wednesday' => ['open_wednesday', 'close_wednesday'],
                'Thursday' => ['open_thursday', 'close_thursday'],
                'Friday' => ['open_friday', 'close_friday'],
                'Saturday' => ['open_saturday', 'close_saturday'],
                'Sunday' => ['open_sunday', 'close_sunday'],
            ];
    }

    public static function todayNameColumns()
    {
        $now = Carbon::now();
        $todayName = $now->dayName; //Monday, Tuesday
        $todayNameColumns = static::weekly()[$todayName];

        return [
                'openColumn' => $todayNameColumns[0],
                'closeColumn' => $todayNameColumns[1],
            ];
    }

    public static function getBufferTime()
    {
        $now = Carbon::now()->addMinutes(config('constants.buffer_time_restaurant'));
        $currentTime = $now->format('H:i');

        return $currentTime;
    }

    public static function readableTimeFormat($time)
    {
        return date("h:i A", strtotime($time));
    }

    public static function timeDuration()
    {
        $durations = [];
        for ($i = 0; $i <= 3; $i++) {
            for ($j = 0; $j <= 45; $j += 15) {
                if ($i === 0 && $j === 0 || $i === 3 && $j !== 0) {
                    //do nothing
                } else {
                    
                    //get string for hours
                    switch ($i) {
                        case 0:
                            $hours = "";
                            break;
                        case 1:
                            $hours = "1 hour";
                            break;
                        default:
                            $hours = $i . " hours";
                            break;
                    }
        
                    //get string for minutes
                    switch ($j) {
                        case 0:
                            $minutes = "";
                            break;
                        default:
                            $minutes = $j . " minutes";
                            break;
                    }
        
                    //$value = ($hours * 60) + $minutes;
                    $value = "";
                    if($hours != ""){
                        $value = $hours;
                    }
                    if($minutes != ""){
                        $value .= $hours != "" ? " " . $minutes : $minutes;
                    }
                    array_push($durations, $value);
                }
            }
        }

        return $durations;
    }

    public static function getLocationName(){
        if(session('location')){
            $location = Location::find(session('location'));
            return $location->location;
        }

        return "Delivery location";
    }
}
