<?php

namespace App\Http\Middleware;

use Closure;
use App\Settings;
use App\Widgets;

class LoadConfigMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $settings = Settings::where('id', 1)->get();
        $widgets = Widgets::where('id', 1)->get();
        config(['settings' => $settings->toArray()[0]]);
        config(['widgets' => $widgets->toArray()[0]]);

        return $next($request);
    }
}
